/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package foxmedika;

import foxmedika.Controller.TransferDatabase;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextInputDialog;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import org.controlsfx.control.HiddenSidesPane;

/**
 *
 * @author Mukhtar
 */
public class Foxmedika extends Application {
    
    @Override
    public void start(Stage primaryStage) throws IOException {
        
        primaryStage = new Stage(StageStyle.UNDECORATED);
        primaryStage.resizableProperty().setValue(Boolean.FALSE);
//        TextInputDialog tid = new TextInputDialog();
//        tid.showAndWait();
////        String ipaddress = JOptionPane.showInputDialog("Masukan IP address : ");
//        String ipaddress = tid.getResult();
//        System.out.println(ipaddress);
//        TransferDatabase td = new TransferDatabase();
//        td.CreateConnection(ipaddress);
//        Parent root = FXMLLoader.load(getClass().getResource("FXML/TransaksiBeliObatManager.fxml"));
        Parent root = FXMLLoader.load(getClass().getResource("FXML/Preloader.fxml"));
        Scene scene = new Scene(root);
        primaryStage.initStyle(StageStyle.TRANSPARENT);
        scene.setFill(null);
        primaryStage.setTitle("Foxmedika");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
