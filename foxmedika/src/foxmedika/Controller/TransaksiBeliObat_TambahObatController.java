/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package foxmedika.Controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;

/**
 * FXML Controller class
 *
 * @author Mukhtar
 */
public class TransaksiBeliObat_TambahObatController implements Initializable {
    @FXML
    private JFXComboBox<String> IDObatBox;
    @FXML
    private Label NamaLabel;
    @FXML
    private Label JenisLabel;
    @FXML
    private Label HargaLabel;
    @FXML
    private JFXTextField JumlahField;
    @FXML
    private JFXComboBox<String> SupplierBox;
    @FXML
    private JFXButton TambahObat;
    @FXML
    private AnchorPane PaneAwal;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        dengarkan();
        isi();
    }    

    @FXML
    private void addObat(ActionEvent event) {
        ForTable temp = new ForTable(IDObatBox.getValue(), NamaLabel.getText(), JenisLabel.getText(), HargaLabel.getText(), JumlahField.getText(), SupplierBox.getValue());
        
    }
    
    private void dengarkan() {
        IDObatBox.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String> () {

            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                NamaLabel.setText(dataNamaObat.get(IDObatBox.getSelectionModel().getSelectedIndex()));
                JenisLabel.setText(dataJenisObat.get(IDObatBox.getSelectionModel().getSelectedIndex()));
                HargaLabel.setText(""+dataHargaObat.get(IDObatBox.getSelectionModel().getSelectedIndex()));
            }
        });
    }
    
    ArrayList<String> dataNamaObat = new ArrayList<>();
    ArrayList<String> dataJenisObat = new ArrayList<>();
    ArrayList<Integer> dataHargaObat = new ArrayList<>();
    
    private void isi() {
        ObservableList<String> dataIDObat = FXCollections.observableArrayList();
        ObservableList<String> dataSupplier = FXCollections.observableArrayList();
        
        for (int i = 0; i < 3; i++) {
            dataIDObat.add(""+i);
            dataNamaObat.add("Obat-"+i);
            dataJenisObat.add("Jenis-"+i);
            dataHargaObat.add(i*1000000);
            dataSupplier.add("Supplier-"+i);
        }
        IDObatBox.setItems(dataIDObat);
        SupplierBox.setItems(dataSupplier);
    }
}
