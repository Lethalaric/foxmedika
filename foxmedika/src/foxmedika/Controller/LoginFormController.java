/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package foxmedika.Controller;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCombination;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javax.swing.JOptionPane;
import org.controlsfx.control.Notifications;

/**
 * FXML Controller class
 *
 * @author Mukhtar
 */
public class LoginFormController implements Initializable {
    @FXML
    private TextField txtusername;
    @FXML
    private PasswordField txtpassword;
    @FXML
    private Button button;
    @FXML
    private TextField IPAddressServer;
    @FXML
    private AnchorPane PaneAwal;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void Masuk(ActionEvent event) {
        System.out.println(txtusername.getText());
        System.out.println(txtpassword.getText());
        Stage primaryStage = new Stage();
        YourSalvation ys = new YourSalvation();
        System.out.println("1");
        ys.setPreference(IPAddressServer.getText());
        Stage stage = (Stage) button.getScene().getWindow();
        Parent root = null;
        int counter = 0;    
        
        try {
            TransferDatabase td = new TransferDatabase();
            td.CreateConnection(ys.getPreference());
            
        } catch (Exception e) {
            Notifications not = Notifications.create();
            not.title("IP Address tidak dapat tersambung");
            not.graphic(new Label("Periksa kembali IP Address anda"));
            not.position(Pos.CENTER);
            not.show();
            counter = 1;
            IPAddressServer.setText("");
        }

        if (((txtusername.getText().equals("manager") && txtpassword.getText().equals("manager")) || (txtusername.getText().equals("kasir") && txtpassword.getText().equals("kasir"))) && counter == 0) {
            if (txtusername.getText().equals("manager") && txtpassword.getText().equals("manager")) {

                try {
                    root = FXMLLoader.load(getClass().getResource("/foxmedika/FXML/TransaksiBeliObatManager.fxml"));
                } catch (IOException ex) {
                    Logger.getLogger(LoginFormController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }else if (txtusername.getText().equals("kasir") && txtpassword.getText().equals("kasir")) {

                try {
                    root = FXMLLoader.load(getClass().getResource("/foxmedika/FXML/TransaksiBeliObat.fxml"));
                } catch (IOException ex) {
                    Logger.getLogger(LoginFormController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

            stage.close();
            Scene scene = new Scene(root);
    //        scene.getStylesheets().add(getClass().getResource("FXML/style/application.css").toExternalForm());
            primaryStage.setScene(scene);
            primaryStage.setResizable(false);
            primaryStage.initStyle(StageStyle.UNDECORATED);
            primaryStage.setTitle("Fox Medika");
    //        primaryStage.setFullScreen(true);
            primaryStage.setFullScreenExitKeyCombination(KeyCombination.NO_MATCH);
            primaryStage.getIcons().add(new Image("file:src/FXML/icon/icon.png"));
            primaryStage.show();
        } else if (counter == 0){
            Notifications not = Notifications.create();
            not.title("Username atau Password salah");
            not.graphic(new Label("Kesalahan pada username atau password, pastikan username dan password benar. (Besar kecil huruf berlaku)"));
            not.position(Pos.CENTER);
            not.show();

            txtpassword.setText("");
            txtusername.setText("");
        }
            
    }
    
}
