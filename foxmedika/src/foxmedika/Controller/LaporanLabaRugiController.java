/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package foxmedika.Controller;

import com.jfoenix.controls.JFXButton;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Side;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import org.controlsfx.control.HiddenSidesPane;

/**
 * FXML Controller class
 *
 * @author Mukhtar
 */
public class LaporanLabaRugiController implements Initializable {
    
    @FXML
    private HiddenSidesPane pane;
    @FXML
    private JFXButton toOpname;
    @FXML
    private JFXButton toJualBeli;
    @FXML
    private JFXButton toLabaRugi;
    @FXML
    private JFXButton toStatistik;
    @FXML
    private JFXButton toAkun;
    @FXML
    private JFXButton toNeraca;
    @FXML
    private JFXButton Profil;
    @FXML
    private JFXButton toTransaksiBeliObat;
    @FXML
    private JFXButton Home;
    @FXML
    private JFXButton GetOut;
    @FXML
    private Pane PaneAwal;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        
    }    
    

    @FXML
    private void handleMouseClicked(MouseEvent event) {
        if (pane.getPinnedSide() != null) {
            pane.setPinnedSide(null);
        } else {
            pane.setPinnedSide(Side.LEFT);
        }
    }

    @FXML
    private void GotoProfil(MouseEvent event) {
        PleaseDontOpen pdo = new PleaseDontOpen();
        pdo.gotoProfil();
    }

    @FXML
    private void GotoHome(MouseEvent event) {
        PleaseDontOpen pdo = new PleaseDontOpen();
        pdo.gotoScene(this.getClass().toString(), PaneAwal);
        
    }
    
    @FXML
    private void Goto(MouseEvent event) {
        PleaseDontOpen pdo = new PleaseDontOpen();
        pdo.gotoScene(event.getSource().toString(), PaneAwal);
    }

    @FXML
    private void Keluar(MouseEvent event) {
        PleaseDontOpen pdo = new PleaseDontOpen();
        pdo.gotoLoginForm(PaneAwal);
    }
    
}
