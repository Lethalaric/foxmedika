/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package foxmedika.Controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXSlider;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.controls.JFXTreeTableView;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Side;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.util.converter.LocalDateStringConverter;
import javax.swing.JOptionPane;
import org.controlsfx.control.HiddenSidesPane;
import org.controlsfx.dialog.ExceptionDialog;

/**
 * FXML Controller class
 *
 * @author Mukhtar
 */
public class TransaksiBarangToKasirController implements Initializable {
    @FXML
    private HiddenSidesPane pane;
    @FXML
    private ScrollPane SP;
    @FXML
    private TableView<ForTable> Tabel;
    @FXML
    private JFXButton toBarangKasir;
    @FXML
    private JFXButton toBeliObat;
    @FXML
    private JFXButton toInputResep;
    @FXML
    private JFXButton toPenjualanObat;
    @FXML
    private JFXButton toPreOrder;
    @FXML
    private JFXButton toRacikResep;
    @FXML
    private JFXSlider slide;
    @FXML
    private JFXTextField slideText;
    @FXML
    private TableColumn<ForTable, String> IDObat;
    @FXML
    private TableColumn<ForTable, String> KolomNamaObat;
    @FXML
    private TableColumn<ForTable, String> KolomJenisObat;
    @FXML
    private TableColumn<ForTable, String> KolomTanggalMasuk;
    @FXML
    private TableColumn<ForTable, String> KolomJumlahObat;
    @FXML
    private TableColumn<ForTable, String> KolomHarga;
    @FXML
    private Label LabelHarga;
    @FXML
    private JFXButton SubmitButton;
    @FXML
    private JFXTextField IDObatField;
    @FXML
    private JFXDatePicker Date;
    @FXML
    private JFXButton Profil;
    @FXML
    private JFXButton Home;
    @FXML
    private JFXButton GetOut;
    @FXML
    private Pane PaneAwal;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        Tabel.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        IDObat.setCellValueFactory(new PropertyValueFactory<ForTable, String>("IDAkun"));
        KolomNamaObat.setCellValueFactory(new PropertyValueFactory<ForTable, String>("Alamat"));
        KolomJenisObat.setCellValueFactory(new PropertyValueFactory<ForTable, String>("Email"));
        KolomTanggalMasuk.setCellValueFactory(new PropertyValueFactory<ForTable, String>("Foto"));
        KolomJumlahObat.setCellValueFactory(new PropertyValueFactory<ForTable, String>("JenisKelamin"));
        KolomHarga.setCellValueFactory(new PropertyValueFactory<ForTable, String>("Jabatan"));
        binding();
        isiData();
        Dengarkan();
    }    

    @FXML
    private void handleMouseClicked(MouseEvent event) {
        if (pane.getPinnedSide() != null) {
            pane.setPinnedSide(null);
        } else {
            pane.setPinnedSide(Side.LEFT);
        }
    }
    
    private void binding() {
//        slideText.textProperty().bindBidirectional(new SimpleStringProperty(""+slide.getValue()));
        slide.valueProperty().addListener(new ChangeListener(){
            @Override
            public void changed(ObservableValue arg0, Object arg1, Object arg2) {
                slideText.textProperty().setValue(String.valueOf((int) slide.getValue()));
            }
        });
        slideText.textProperty().addListener(new ChangeListener() {

            @Override
            public void changed(ObservableValue observable, Object oldValue, Object newValue) {
                try {
                    if (slideText.getText().equals("")) {
                        slideText.setText("0");
                    }
                    if (Integer.parseInt(slideText.getText()) > 1000) {
                        slideText.setText("1000");
                    }
                    slide.setValue(Integer.parseInt(slideText.getText()));
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(null, "Hanya dapat mengisi angka", "Warning", 1);
                    slide.setValue(0);
                    slideText.setText("0");
                }
            }
        });
    }
    
    int counter = 0;
    ObservableList<ForTable> ol = FXCollections.observableArrayList();
    
    @FXML
    private void doSubmit() {
        try {
            if(IDObatField.getText().equals("")) {
                throw new NullPointerException();
            }
            int tempHarga = (int) (Math.random() * 1000000);
            LocalDate ld = Date.getValue();
            ForTable temp = new ForTable(""+counter, IDObatField.getText(), " ", Date.getValue().toString() , ""+slide.getValue(), ""+tempHarga);
            counter++;
            ol.add(temp);
            Tabel.setItems(ol);
        } catch (NullPointerException e) {
            e = new NullPointerException("ID obat, Nama obat dan Tanggal tidak boleh null");
            ExceptionDialog ed = new ExceptionDialog(e);
            ed.setTitle("error ");
            ed.show();
        } 
    }
    
    private void Dengarkan() {
//        IDObatField.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
//
//            @Override
//            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
//                NamaObatField.getSelectionModel().select(IDObatField.getSelectionModel().getSelectedIndex());
//            }
//        });
//        NamaObatField.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
//
//            @Override
//            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
//                IDObatField.getSelectionModel().select(NamaObatField.getSelectionModel().getSelectedIndex());
//            }
//        });
    }
    
    private void isiData() {
        ObservableList<String> IDData = FXCollections.observableArrayList();
        for (int i = 0; i < 3; i++) {
            IDData.add(""+i);
        }
        ObservableList<String> NamaData = FXCollections.observableArrayList();
        for (int i = 0; i < 3; i++) {
            NamaData.add("Nama-"+i);
        }
//        IDObatField.setItems(IDData);
//        NamaObatField.setItems(NamaData);
    }

    @FXML
    private void GotoProfil(MouseEvent event) {
        PleaseDontOpen pdo = new PleaseDontOpen();
        pdo.gotoProfil();
    }

    @FXML
    private void GotoHome(MouseEvent event) {
        PleaseDontOpen pdo = new PleaseDontOpen();
        pdo.gotoScene(this.getClass().toString(), PaneAwal);
        
    }
    
    @FXML
    private void Goto(MouseEvent event) {
        PleaseDontOpen pdo = new PleaseDontOpen();
        pdo.gotoScene(event.getSource().toString(), PaneAwal);
    }

    @FXML
    private void Keluar(MouseEvent event) {
        PleaseDontOpen pdo = new PleaseDontOpen();
        pdo.gotoLoginForm(PaneAwal);
    }
}
