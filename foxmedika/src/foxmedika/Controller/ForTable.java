/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package foxmedika.Controller;

import com.jfoenix.controls.JFXTextField;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.control.TextField;

/**
 *
 * @author Mukhtar
 */
public class ForTable {
    private String IDAkun = "";
    private String Alamat = "";
    private String Email = "";
    private String Foto = "";
    private String JenisKelamin = "";
    private String Jabatan = "";
    private String NomorKontak = "";
    private String LastLogin = "";
    private String LastLogout = "";
    private StringProperty Nama = new SimpleStringProperty("");
    private String Password = "";
    private String TTL = "";
    private String Username = "";
    private String photo = "";
    private TextField fckthis;
    

    public ForTable() {
        IDAkun = "";
        Alamat = "";
        Email = "";
        Foto = "";
        JenisKelamin = "";
        Jabatan = "";
        NomorKontak = "";
        LastLogin = "";
        LastLogout = "";
        Nama = new SimpleStringProperty("");
        Password = "";
        TTL = "";
        Username = "";
    }
    
    public ForTable(String s1, String s2, String s3, String s4, String s5, String s6, String s7, String s8, String s9, String s10, String s11, String s12, String s13) {
        IDAkun = s1;
        Alamat = s2;
        Email = s3;
        Foto = s4;
        JenisKelamin = s5;
        Jabatan = s6;
        NomorKontak = s7;
        LastLogin = s8;
        LastLogout = s9;
        Nama.set(s10);
        Password = s11;
        TTL = s12;
        Username = s13;
    }
    
    public ForTable(String s1, String s2, String s3, String s4, String s5, String s6, String s7) {
        IDAkun = s1;
        Alamat = s2;
        Email = s3;
        Foto = s4;
        JenisKelamin = s5;
        Jabatan = s6;
        NomorKontak = s7;
    }
    
    public ForTable(String s1, String s2, String s3, String s4, String s5, String s6, String s7, String s8) {
//        IDAkun = s1;
        Nama.set(s1);
        Alamat = s2;
        Email = s3;
        Foto = s4;
        JenisKelamin = s5;
        Jabatan = s6;
        NomorKontak = s7;
        LastLogin = s8;
    }
    
    public ForTable(String s1, String s2, String s3, String s4, String s5, String s6, String s7, String s8, String s9) {
//        IDAkun = s1;
        Nama.set(s1);
        Alamat = s2;
        Email = s3;
        Foto = s4;
        JenisKelamin = s5;
        Jabatan = s6;
        NomorKontak = s7;
        LastLogin = s8;
        LastLogout = s9;
    }
    
    public ForTable(String s1, String s2, String s3, String s4, String s5, String s6, String s7, String s8, String s9, String s10) {
//        IDAkun = s1;
        Nama.set(s1);
        Alamat = s2;
        Email = s3;
        Foto = s4;
        JenisKelamin = s5;
        Jabatan = s6;
        NomorKontak = s7;
        LastLogin = s8;
        LastLogout = s9;
        fckthis = new TextField(s10);
    }
    
    public ForTable(String s1, String s2, String s3, String s4, String s5, String s6) {
        IDAkun = s1;
        Alamat = s2;
        Email = s3;
        Foto = s4;
        JenisKelamin = s5;
        Jabatan = s6;
    }
    
    public TextField getField() {
        return fckthis;
    }
    
    public void setField(String x) {
        fckthis = new JFXTextField(x);
        fckthis.setEditable(true);
    }
    
    public void setphoto(String value) {
        this.photo = value;
    } 
    
    public String getPhoto() {
        return this.photo;
    }

    public String getIDAkun() {
        return IDAkun;
    }

    public void setIDAkun(String IDAkun) {
        this.IDAkun = IDAkun;
    }

    public String getAlamat() {
        return Alamat;
    }

    public void setAlamat(String Alamat) {
        this.Alamat = Alamat;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String Email) {
        this.Email = Email;
    }

    public String getFoto() {
        return Foto;
    }

    public void setFoto(String Foto) {
        this.Foto = Foto;
    }

    public String getJenisKelamin() {
        return JenisKelamin;
    }

    public void setJenisKelamin(String JenisKelamin) {
        this.JenisKelamin = JenisKelamin;
    }

    public String getJabatan() {
        return Jabatan;
    }

    public void setJabatan(String Jabatan) {
        this.Jabatan = Jabatan;
    }

    public String getNomorKontak() {
        return NomorKontak;
    }

    public void setNomorKontak(String NomorKontak) {
        this.NomorKontak = NomorKontak;
    }

    public String getLastLogin() {
        return LastLogin;
    }

    public void setLastLogin(String LastLogin) {
        this.LastLogin = LastLogin;
    }

    public String getLastLogout() {
        return LastLogout;
    }

    public void setLastLogout(String LastLogout) {
        this.LastLogout = LastLogout;
    }
    
    public String getNama() {
        return Nama.get();
    }

    public StringProperty getNama2() {
        return Nama;
    }

    public void setNama(String Nama) {
        this.Nama.set(Nama);
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String Password) {
        this.Password = Password;
    }

    public String getTTL() {
        return TTL;
    }

    public void setTTL(String TTL) {
        this.TTL = TTL;
    }

    public String getUsername() {
        return Username;
    }

    public void setUsername(String Username) {
        this.Username = Username;
    }
    
}
