/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package foxmedika.Controller;

import java.util.prefs.Preferences;

/**
 *
 * @author Mukhtar
 */
public class YourSalvation {
    
    private Preferences pref;
    private String IpAddress;
    
    public YourSalvation() {
        pref = Preferences.userRoot().node(this.getClass().getName());
        IpAddress = "127.0.0.0";
    }
    
    public void setPreference(String value) {
        try {
            pref.remove(IpAddress);
        } catch (Exception e) {
            
        }
        pref.put(IpAddress, value);
    }
    
    public String getPreference() {
        return pref.get(IpAddress, null);
    }
}
