/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package foxmedika.Controller;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Side;
import javafx.scene.control.ScrollPane;
import javafx.scene.input.MouseEvent;
import org.controlsfx.control.HiddenSidesPane;
import com.jfoenix.controls.*;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.Pane;

/**
 * FXML Controller class
 *
 * @author Mukhtar
 */
public class LaporanJualBeliController implements Initializable {
    @FXML
    private HiddenSidesPane pane;
    @FXML
    private ScrollPane SP;
    @FXML
    private TableView<ForTable> Tabel;
    @FXML
    private JFXButton toOpname;
    @FXML
    private JFXButton toJualBeli;
    @FXML
    private JFXButton toLabaRugi;
    @FXML
    private JFXButton toStatistik;
    @FXML
    private JFXButton toAkun;
    @FXML
    private JFXButton toNeraca;
    @FXML
    private TableColumn<ForTable, String> IDObat;
    @FXML
    private TableColumn<ForTable, String> NamaObat;
    @FXML
    private TableColumn<ForTable, String> JenisObat;
    @FXML
    private TableColumn<ForTable, String> HargaSatuan;
    @FXML
    private TableColumn<ForTable, String> JumlahDikasir;
    @FXML
    private TableColumn<ForTable, String> JumlahDigudang;
    @FXML
    private TableColumn<ForTable, String> TerjualTerbeli;
    
    private ObservableList<ForTable> TableData = FXCollections.observableArrayList();
    
//    private ArrayList<String> dummyData1 = new ArrayList<>(Arrays.asList("Nunc vel est ullamcorper, egestas leo nec, scelerisque mauris. Sed aliquam, quam fringilla ultrices faucibus, turpis est dictum nulla, quis viverra orci diam ut ligula. Quisque consectetur felis sit amet pharetra ultrices. Donec congue sodales sodales. In scelerisque est nec lacus aliquam, quis vehicula sapien convallis. Maecenas non vulputate sapien, ut lacinia urna. Duis finibus odio aliquam lacus rhoncus feugiat. Vivamus condimentum, risus a dapibus finibus, est lorem ullamcorper lorem, vulputate vulputate arcu mi eget felis. Donec ultrices quam sapien, non dictum nibh facilisis ut. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Mauris ultrices facilisis orci, eu eleifend augue tincidunt non. Suspendisse elementum nisi sed massa volutpat, quis vehicula lectus pulvinar. Mauris scelerisque convallis nunc non faucibus. Aliquam tincidunt diam at augue bibendum ullamcorper eu quis quam. Nulla tincidunt ex blandit, laoreet purus a, vehicula sem. Quisque quis odio lorem. Etiam convallis scelerisque quam, vel lobortis libero efficitur at. Integer eget interdum dui, id sagittis est. Praesent ut egestas neque. Cras a molestie sem. Nullam non eleifend libero. Integer vitae purus vitae sapien molestie aliquam. Integer dapibus tortor ac metus pharetra molestie. Suspendisse mi magna, sodales in nibh nec, consequat placerat libero. Proin eget dolor nibh. Nulla quis maximus ligula. Aliquam sed mi dolor. Nulla eu lacus ligula.".split(" ")));
//    private ArrayList<String> dummyData2 = new ArrayList<>(Arrays.asList("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent ac eros a lectus sollicitudin efficitur at ac nisl. Nullam velit neque, vulputate id ullamcorper ut, maximus quis elit. Mauris at mauris id tortor luctus congue. Aliquam erat volutpat. In fermentum neque in enim tempus, in ullamcorper neque aliquet. Sed molestie mattis metus, nec blandit erat accumsan id. Duis sit amet rhoncus nulla. Interdum et malesuada fames ac ante ipsum primis in faucibus. Ut lectus mauris, convallis et aliquet quis, blandit at elit. Sed feugiat nunc sed eros fermentum tincidunt. Phasellus a metus urna. Cras neque nunc, posuere ut malesuada quis, faucibus sit amet lacus. Aenean aliquam luctus est, quis vulputate felis aliquet ut. Phasellus condimentum sodales leo. Maecenas a nulla a quam tempus vehicula sit amet quis lorem. Suspendisse bibendum molestie tristique. Quisque sollicitudin nec ipsum finibus porttitor. Pellentesque aliquam tristique metus at finibus. Quisque laoreet velit id urna sagittis faucibus. Ut quis diam volutpat, egestas metus et, tincidunt mauris. Nullam sollicitudin odio dolor, et consectetur nunc tincidunt pretium. Donec consectetur posuere augue. Etiam in porttitor massa, ac scelerisque metus. Pellentesque a metus et nunc pellentesque cursus non at mauris. Proin varius eu sapien vitae faucibus. Pellentesque ac nisi mauris.".split(" ")));
    @FXML
    private JFXButton Profil;
    @FXML
    private JFXButton toTransaksiBeliObat;
    @FXML
    private JFXButton Home;
    @FXML
    private JFXButton GetOut;
    @FXML
    private Pane PaneAwal;
    

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        Tabel.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        IDObat.setCellValueFactory(new PropertyValueFactory<ForTable, String>("IDAkun"));
        NamaObat.setCellValueFactory(new PropertyValueFactory<ForTable, String>("Alamat"));
        JenisObat.setCellValueFactory(new PropertyValueFactory<ForTable, String>("Email"));
        HargaSatuan.setCellValueFactory(new PropertyValueFactory<ForTable, String>("Foto"));
        JumlahDikasir.setCellValueFactory(new PropertyValueFactory<ForTable, String>("JenisKelamin"));
        JumlahDigudang.setCellValueFactory(new PropertyValueFactory<ForTable, String>("Jabatan"));
        TerjualTerbeli.setCellValueFactory(new PropertyValueFactory<ForTable, String>("NomorKontak"));
        isiPilihan();
        setTablePenjualan();
        keepOn();
    }    
    
    private Thread t = new Thread();
    
    private void keepOn() {
        t = new Thread() {
            public void run() {
                while(true) {
                    if (Pilihan.getSelectionModel().getSelectedIndex() == 0) {
                        setTablePenjualan();
                    }
                    if (Pilihan.getSelectionModel().getSelectedIndex() == 1) {
                        setTablePembelian();
                    }
                    try {
                        Thread.sleep(10000);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(LaporanAkunController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        };
        t.start();
    }

    @FXML
    private void handleMouseClicked(MouseEvent event) {
        if (pane.getPinnedSide() != null) {
            pane.setPinnedSide(null);
        } else {
            pane.setPinnedSide(Side.LEFT);
        }
    }
    
    private void setTablePenjualan() {
        TableData = FXCollections.observableArrayList();
        Tabel.getItems().clear();
        TerjualTerbeli.setText("Terjual");
        PleaseDontOpen pdo = new PleaseDontOpen();
        ArrayList<String> dummyData1 = pdo.RefreshTable("Select * from TableBaru", new String[]{"a1", "a2", "a3", "a4", "a5", "a6", "a7"});
        for (int i=0; i < dummyData1.size()-7; i+=7) {
            ForTable temp = new ForTable(""+dummyData1.get(i), ""+dummyData1.get(i+1), ""+dummyData1.get(i+2), ""+dummyData1.get(i+3), ""+dummyData1.get(i+4), ""+dummyData1.get(i+5), ""+dummyData1.get(i+6));
            TableData.add(temp);
        }
        Tabel.getItems().addAll(TableData);
    }
    
    private void setTablePembelian() {
        TableData = FXCollections.observableArrayList();
        Tabel.getItems().clear();
        TerjualTerbeli.setText("Terjual");
        PleaseDontOpen pdo = new PleaseDontOpen();
        ArrayList<String> dummyData2 = pdo.RefreshTable("Select * from TableBaru", new String[]{"a1", "a2", "a3", "a4", "a5", "a6", "a7"});
        for (int i=0; i < dummyData2.size()-7; i+=7) {
            ForTable temp = new ForTable(""+dummyData2.get(i), ""+dummyData2.get(i+1), ""+dummyData2.get(i+2), ""+dummyData2.get(i+3), ""+dummyData2.get(i+4), ""+dummyData2.get(i+5), ""+dummyData2.get(i+6));
            TableData.add(temp);
        }
        Tabel.getItems().addAll(TableData);
    }
    
    @FXML
    private JFXComboBox<String> Pilihan;
    private void isiPilihan() {
        ObservableList<String> data = FXCollections.observableArrayList("Penjualan Hari Ini","Pembelian Hari Ini");
        if (Pilihan != null) {
            Pilihan.getItems().clear();
        }
        Pilihan.getItems().addAll(data);
    }
    
    @FXML
    public void Pilih() {
        if (Pilihan.getSelectionModel().getSelectedIndex() == 0) {
            setTablePenjualan();
        } else if(Pilihan.getSelectionModel().getSelectedIndex() == 1) {
            setTablePembelian();
        }
    }

    @FXML
    private void GotoProfil(MouseEvent event) {
        PleaseDontOpen pdo = new PleaseDontOpen();
        pdo.gotoProfil();
    }

    @FXML
    private void GotoHome(MouseEvent event) {
        t.stop();
        PleaseDontOpen pdo = new PleaseDontOpen();
        pdo.gotoScene(this.getClass().toString(), PaneAwal);
        
    }
    
    @FXML
    private void Goto(MouseEvent event) {
        t.stop();
        PleaseDontOpen pdo = new PleaseDontOpen();
        pdo.gotoScene(event.getSource().toString(), PaneAwal);
    }

    @FXML
    private void Keluar(MouseEvent event) {
        t.stop();
        PleaseDontOpen pdo = new PleaseDontOpen();
        pdo.gotoLoginForm(PaneAwal);
    }
}
