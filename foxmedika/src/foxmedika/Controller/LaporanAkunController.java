/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package foxmedika.Controller;

import com.jfoenix.controls.*;
import foxmedika.Foxmedika;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.geometry.Side;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import org.controlsfx.control.HiddenSidesPane;

public class LaporanAkunController implements Initializable {

    @FXML
    private HiddenSidesPane pane;
    @FXML
    private ScrollPane SP;
    @FXML
    private JFXButton toOpname;
    @FXML
    private JFXButton toJualBeli;
    @FXML
    private JFXButton toLabaRugi;
    @FXML
    private JFXButton toStatistik;
    @FXML
    private JFXButton toAkun;
    @FXML
    private JFXButton toNeraca;
    @FXML
    private TableColumn<ForTable, String> KolomIDAkun;
    @FXML
    private TableColumn<ForTable, String> KolomNama;
    @FXML
    private TableColumn<ForTable, String> KolomTTL;
    @FXML
    private TableColumn<ForTable, String> KolomJK;
    @FXML
    private TableColumn<ForTable, String> KolomKontak;
    @FXML
    private TableColumn<ForTable, String> KolomAlamat;
    @FXML
    private TableColumn<ForTable, String> KolomEmail;
    @FXML
    private TableColumn<ForTable, String> KolomFoto;
    @FXML
    private TableColumn<ForTable, String> KolomUsername;
    @FXML
    private TableColumn<ForTable, String> KolomPassword;
    @FXML
    private TableColumn<ForTable, String> KolomJabatan;
    @FXML
    private TableColumn<ForTable, String> KolomLastLogin;
    @FXML
    private TableColumn<ForTable, String> KolomLastLogout;
    @FXML
    private TableView<ForTable> TabelAkun;
    
    private ObservableList<ForTable> TableData = FXCollections.observableArrayList();
    
//    private ArrayList<String> dummyData = new ArrayList<>(Arrays.asList("Nunc vel est ullamcorper, egestas leo nec, scelerisque mauris. Sed aliquam, quam fringilla ultrices faucibus, turpis est dictum nulla, quis viverra orci diam ut ligula. Quisque consectetur felis sit amet pharetra ultrices. Donec congue sodales sodales. In scelerisque est nec lacus aliquam, quis vehicula sapien convallis. Maecenas non vulputate sapien, ut lacinia urna. Duis finibus odio aliquam lacus rhoncus feugiat. Vivamus condimentum, risus a dapibus finibus, est lorem ullamcorper lorem, vulputate vulputate arcu mi eget felis. Donec ultrices quam sapien, non dictum nibh facilisis ut. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Mauris ultrices facilisis orci, eu eleifend augue tincidunt non. Suspendisse elementum nisi sed massa volutpat, quis vehicula lectus pulvinar. Mauris scelerisque convallis nunc non faucibus. Aliquam tincidunt diam at augue bibendum ullamcorper eu quis quam. Nulla tincidunt ex blandit, laoreet purus a, vehicula sem. Quisque quis odio lorem. Etiam convallis scelerisque quam, vel lobortis libero efficitur at. Integer eget interdum dui, id sagittis est. Praesent ut egestas neque. Cras a molestie sem. Nullam non eleifend libero. Integer vitae purus vitae sapien molestie aliquam. Integer dapibus tortor ac metus pharetra molestie. Suspendisse mi magna, sodales in nibh nec, consequat placerat libero. Proin eget dolor nibh. Nulla quis maximus ligula. Aliquam sed mi dolor. Nulla eu lacus ligula.".split(" "))) ;
    @FXML
    private JFXTextField searchField;
    @FXML
    private Pane PaneAwal;
    @FXML
    private JFXButton Profil;
    @FXML
    private JFXButton toTransaksiBeliObat;
    @FXML
    private JFXButton Home;
    @FXML
    private JFXButton GetOut;

    @FXML
    private void handleMouseClicked(MouseEvent event) {
      if (pane.getPinnedSide() != null) {
        pane.setPinnedSide(null);
      } else {
        pane.setPinnedSide(Side.LEFT);
      }
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
      // TODO
        TabelAkun.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        KolomIDAkun.setCellValueFactory(new PropertyValueFactory<ForTable, String>("IDAkun"));
        KolomAlamat.setCellValueFactory(new PropertyValueFactory<ForTable, String>("Alamat"));
        KolomEmail.setCellValueFactory(new PropertyValueFactory<ForTable, String>("Email"));
        KolomFoto.setCellValueFactory(new PropertyValueFactory<ForTable, String>("photo"));
        KolomJK.setCellValueFactory(new PropertyValueFactory<ForTable, String>("JenisKelamin"));
        KolomJabatan.setCellValueFactory(new PropertyValueFactory<ForTable, String>("Jabatan"));
        KolomKontak.setCellValueFactory(new PropertyValueFactory<ForTable, String>("NomorKontak"));
        KolomLastLogin.setCellValueFactory(new PropertyValueFactory<ForTable, String>("LastLogin"));
        KolomLastLogout.setCellValueFactory(new PropertyValueFactory<ForTable, String>("LastLogout"));
        KolomNama.setCellValueFactory(new PropertyValueFactory<ForTable, String>("Nama"));
        KolomPassword.setCellValueFactory(new PropertyValueFactory<ForTable, String>("Password"));
        KolomTTL.setCellValueFactory(new PropertyValueFactory<ForTable, String>("TTL"));
        KolomUsername.setCellValueFactory(new PropertyValueFactory<ForTable, String>("Username"));
        setPhoto();
//        setDataTable();
//        bindNama();
        keepOn();
    }
    
    private Thread t = new Thread();
    
    private void keepOn() {
//        Fiber<Void> f = new Fiber<Void>(new SuspendableRunnable() {
//            public void run() throws SuspendExecution, InterruptedException {
//              // your code
//                while(cekcok) {
//                    setDataTable();
//                    if (counter != 0) {
//                        cekcok = false;
//                    }
//                    try {
//                        Strand.sleep(10000);
//                    } catch (InterruptedException ex) {
//                        Logger.getLogger(LaporanAkunController.class.getName()).log(Level.SEVERE, null, ex);
//                    }
//                }
//            }
//        }).start();
//        Task<Void> t = new Task<Void>() {
//
//            @Override
//            public Void call() throws Exception {
//                while(true) {
//                    setDataTable();
//                    try {
//                        Thread.sleep(10000);
//                    } catch (InterruptedException ex) {
//                        Logger.getLogger(LaporanAkunController.class.getName()).log(Level.SEVERE, null, ex);
//                    }
//                }
//            }
//        };
//        new Thread(t).start();
        t = new Thread() {
            public void run() {
                while(true) {
                    setDataTable();
                    try {
                        Thread.sleep(10000);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(LaporanAkunController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        };
        t.start();
    }


     private ArrayList<String> dummyData = new ArrayList<>();
    
      private void setDataTable() {
        TableData = FXCollections.observableArrayList();
        PleaseDontOpen pdo = new PleaseDontOpen();
        dummyData = pdo.RefreshTable("Select * from TableBaru", new String[]{"a1", "a2", "a3", "a4", "a5", "a6", "a7", "a8", "a9", "a10", "a11", "a12", "a13"}, "refresh laporan akun");
        for (int i = 0; i < dummyData.size()-12; i+=13) {
//            System.out.println(dummyData.get(i));
            ForTable temp = new ForTable(""+ dummyData.get(i), ""+ dummyData.get(i+1), ""+ dummyData.get(i+2), ""+ dummyData.get(i+3), ""+ dummyData.get(i+4), ""+ dummyData.get(i+5), ""+ dummyData.get(i+6), ""+ dummyData.get(i+7), ""+ dummyData.get(i+8), ""+ dummyData.get(i+9), ""+ dummyData.get(i+10), ""+ dummyData.get(i+11), ""+ dummyData.get(i+12));
            temp.setphoto("YY2.jpg");
            TableData.add(temp);
        }
        FilteredList<ForTable> FL = new FilteredList<>(TableData, p -> true);
        searchField.textProperty().addListener((Observable, oldValue, newValue)-> {
            FL.setPredicate(ForTable -> {
                setPhoto();
              if (newValue == null || newValue.isEmpty()) {
                      return true;
              }

              String lowerCaseFilter = newValue.toLowerCase();

              if (ForTable.getNama().toLowerCase().indexOf(lowerCaseFilter) != -1) {
                      return true; 
              }
              return false;
            });
        });
        SortedList<ForTable> sorted = new SortedList<>(FL);
        sorted.comparatorProperty().bind(TabelAkun.comparatorProperty());
        TabelAkun.setItems(sorted);
        
        
      }
      
      private void setPhoto() {
          KolomFoto.setCellFactory(new Callback<TableColumn<ForTable,String>,TableCell<ForTable,String>>(){        
            @Override
            public TableCell<ForTable,String> call(TableColumn<ForTable,String> param) {                
                TableCell<ForTable,String> cell = new TableCell<ForTable,String>(){
                    @Override
                    public void updateItem(String item, boolean empty) {                        
                        if(item!=null){                         
                            VBox vbox = new VBox();
                            vbox.setAlignment(Pos.CENTER);

                            ImageView imageview = new ImageView();
                            imageview.setFitHeight(50);
                            imageview.setFitWidth(50);
                            imageview.setImage(new Image(Foxmedika.class.getResource("Image").toString()+"/"+item)); 

//                            box.getChildren().addAll(imageview,vbox); 
                            vbox.getChildren().add(imageview);
                            //SETTING ALL THE GRAPHICS COMPONENT FOR CELL
                            setGraphic(vbox);
                        }
                    }
                };
//                System.out.println(cell.getIndex());               
                return cell;
            }
            
        });        
      }

    @FXML
    private void GotoProfil(MouseEvent event) {
        PleaseDontOpen pdo = new PleaseDontOpen();
        pdo.gotoProfil();
    }

    @FXML
    private void GotoHome(MouseEvent event) {
        t.stop();
        PleaseDontOpen pdo = new PleaseDontOpen();
        pdo.gotoScene(this.getClass().toString(), PaneAwal);
        
    }

    @FXML
    private void Keluar(MouseEvent event) {
        t.stop();
        PleaseDontOpen pdo = new PleaseDontOpen();
        pdo.gotoLoginForm(PaneAwal);
    }
    
      @FXML
    private void Goto(MouseEvent event) {
        t.stop();
        PleaseDontOpen pdo = new PleaseDontOpen();
        pdo.gotoScene(event.getSource().toString(), PaneAwal);
    }
}

