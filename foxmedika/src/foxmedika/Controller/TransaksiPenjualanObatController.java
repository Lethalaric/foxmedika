/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package foxmedika.Controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXSlider;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.controls.JFXTreeTableView;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.geometry.Side;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.Callback;
import javax.swing.JOptionPane;
import org.controlsfx.control.HiddenSidesPane;

/**
 * FXML Controller class
 *
 * @author Mukhtar
 */
public class TransaksiPenjualanObatController implements Initializable {
    @FXML
    private HiddenSidesPane pane;
    @FXML
    private ScrollPane SP;
    @FXML
    private TableView<ForTable> Tabel;
    @FXML
    private JFXButton toBarangKasir;
    @FXML
    private JFXButton toBeliObat;
    @FXML
    private JFXButton toInputResep;
    @FXML
    private JFXButton toPenjualanObat;
    @FXML
    private JFXButton toPreOrder;
    @FXML
    private JFXButton toRacikResep;
    @FXML
    private JFXTextField NamaObat;
    @FXML
    private JFXButton SubmitResep;
    @FXML
    private JFXButton Profil;
    @FXML
    private ScrollPane SP2;
    @FXML
    private TableView<ForTable> Tabel2;
    @FXML
    private JFXButton Beli;
    @FXML
    private TableColumn<ForTable, String> IDObatRow1;
    @FXML
    private TableColumn<ForTable, String> NamaObatRow1;
    @FXML
    private TableColumn<ForTable, String> JumlahRow1;
    @FXML
    private TableColumn<ForTable, String> HargaSatuanRow1;
    @FXML
    private TableColumn<ForTable, String> IDObatRow2;
    @FXML
    private TableColumn<ForTable, String> NamaObarRow2;
    @FXML
    private TableColumn<ForTable, String> JenisObatRow2;
    @FXML
    private TableColumn<ForTable, String> JenisObatRow1;
    @FXML
    private TableColumn<ForTable, String> HargaSatuanRow2;
    @FXML
    private TableColumn<ForTable, String> JumlahRow2;
    @FXML
    private JFXButton Home;
    @FXML
    private JFXButton GetOut;
    @FXML
    private Pane PaneAwal;
    
    final  Map<String, SimpleStringProperty> m2 = new HashMap<>();


    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        isiTabel();
        Tabel.getSelectionModel().select(0);
        KeepOn();
    }    

    @FXML
    private void handleMouseClicked(MouseEvent event) {
        if (pane.getPinnedSide() != null) {
            pane.setPinnedSide(null);
        } else {
            pane.setPinnedSide(Side.LEFT);
        }
    }
    
    private void setJumlahPesanan() {
        
        JumlahRow1.setCellFactory(new Callback<TableColumn<ForTable,String>,TableCell<ForTable,String>>(){        
            @Override
            public TableCell<ForTable,String> call(TableColumn<ForTable,String> param) {                
                TableCell<ForTable,String> cell = new TableCell<ForTable,String>(){
                    @Override
                    public void updateItem(String item, boolean empty) {                        
                        if(item!=null){                         
                            VBox vbox = new VBox();
                            vbox.setAlignment(Pos.CENTER);

                            SimpleStringProperty x = new SimpleStringProperty();
                            
                            try {
                              x  = m2.get(IDObatRow1.getColumns().get(getIndex()));

                            } catch (Exception e) {

                            }
                            JFXTextField textField = new JFXTextField();
                            textField.setText(x.get());
                            final SimpleStringProperty xxxx = x;
                            final SimpleStringProperty newX = new SimpleStringProperty();
                            textField.textProperty().addListener((Observable, oldValue, newValue)-> {
//                              final SimpleStringProperty xxxx = x;
                                autoSave as = new autoSave();
                                as.setX(newValue);
                                newX.set(as.getX());
                                
                                m2.replace(xxxx.get(), newX);
                                
                            });
//                            System.out.println("----------" + m.get(x.get()));
                            textField.setPrefHeight(50);
                            textField.setPrefWidth(50);

                            vbox.getChildren().add(textField);
                            //SETTING ALL THE GRAPHICS COMPONENT FOR CELL
                            setGraphic(vbox);
                        }
                    }
                };           
                return cell;
            }

        });        
    }
    
    private void wtfisthis() {
        JumlahRow2.setCellFactory(new Callback<TableColumn<ForTable,String>,TableCell<ForTable,String>>(){        
          @Override
          public TableCell<ForTable,String> call(TableColumn<ForTable,String> param) {                
              TableCell<ForTable,String> cell = new TableCell<ForTable,String>(){
                  @Override
                  public void updateItem(String item, boolean empty) {                        
                      if(item!=null){                         
                          VBox vbox = new VBox();
                          vbox.setAlignment(Pos.CENTER);

                          JFXTextField textField = new JFXTextField();
                          textField.setPrefHeight(50);
                          textField.setPrefWidth(50);

                          vbox.getChildren().add(textField);
                          //SETTING ALL THE GRAPHICS COMPONENT FOR CELL
                          setGraphic(vbox);
                      }
                  }
              };           
              return cell;
          }

        }); 
    }
    
     private void isiTabel() {
        Tabel.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        Tabel.setEditable(true);
        IDObatRow1.setCellValueFactory(new PropertyValueFactory<ForTable, String>("Nama"));
        IDObatRow2.setCellValueFactory(new PropertyValueFactory<ForTable, String>("Nama"));
        NamaObatRow1.setCellValueFactory(new PropertyValueFactory<ForTable, String>("Email"));
        NamaObarRow2.setCellValueFactory(new PropertyValueFactory<ForTable, String>("Foto"));
        JenisObatRow1.setCellValueFactory(new PropertyValueFactory<ForTable, String>("JenisKelamin"));
        JenisObatRow2.setCellValueFactory(new PropertyValueFactory<ForTable, String>("Jabatan"));
        JumlahRow1.setCellValueFactory(new PropertyValueFactory<ForTable, String>("NomorKontak"));
        JumlahRow2.setCellValueFactory(new PropertyValueFactory<ForTable, String>("NomorKontak"));
        HargaSatuanRow1.setCellValueFactory(new PropertyValueFactory<ForTable, String>("LastLogin"));
        HargaSatuanRow2.setCellValueFactory(new PropertyValueFactory<ForTable, String>("LastLogin"));
        setDataTable();
        setJumlahPesanan();
    }
     
    private ObservableList<ForTable> TableData = FXCollections.observableArrayList();
    
//    private ArrayList<String> dummyData = new ArrayList<>(Arrays.asList("Nunc vel est ullamcorper, egestas leo nec, scelerisque mauris. Sed aliquam, quam fringilla ultrices faucibus, turpis est dictum nulla, quis viverra orci diam ut ligula. Quisque consectetur felis sit amet pharetra ultrices. Donec congue sodales sodales. In scelerisque est nec lacus aliquam, quis vehicula sapien convallis. Maecenas non vulputate sapien, ut lacinia urna. Duis finibus odio aliquam lacus rhoncus feugiat. Vivamus condimentum, risus a dapibus finibus, est lorem ullamcorper lorem, vulputate vulputate arcu mi eget felis. Donec ultrices quam sapien, non dictum nibh facilisis ut. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Mauris ultrices facilisis orci, eu eleifend augue tincidunt non. Suspendisse elementum nisi sed massa volutpat, quis vehicula lectus pulvinar. Mauris scelerisque convallis nunc non faucibus. Aliquam tincidunt diam at augue bibendum ullamcorper eu quis quam. Nulla tincidunt ex blandit, laoreet purus a, vehicula sem. Quisque quis odio lorem. Etiam convallis scelerisque quam, vel lobortis libero efficitur at. Integer eget interdum dui, id sagittis est. Praesent ut egestas neque. Cras a molestie sem. Nullam non eleifend libero. Integer vitae purus vitae sapien molestie aliquam. Integer dapibus tortor ac metus pharetra molestie. Suspendisse mi magna, sodales in nibh nec, consequat placerat libero. Proin eget dolor nibh. Nulla quis maximus ligula. Aliquam sed mi dolor. Nulla eu lacus ligula.".split(" "))) ;
    
    
    private void setDataTable() {
        TableData = FXCollections.observableArrayList();
        PleaseDontOpen pdo = new PleaseDontOpen();
        ArrayList<String> dummyData = pdo.RefreshTable("Select * from TableBaru", new String[]{"a1", "a2", "a3", "a4", "a5", "a6", "a7", "a8", "a9", "a10"});
        for (int i = 0; i < dummyData.size()-8; i+=9) {
//            System.out.println(dummyData.get(i));
            ForTable temp = new ForTable(""+ dummyData.get(i), ""+ dummyData.get(i+1), ""+ dummyData.get(i+2), ""+ dummyData.get(i+3), ""+ dummyData.get(i+4), ""+ dummyData.get(i+5), ""+ dummyData.get(i+6), ""+ dummyData.get(i+7), ""+ dummyData.get(i+8), "cek123" );
//            temp.setphoto("YY2.jpg");
//            temp.setField("zxcvbnm");
            TableData.add(temp);
        }
        
        
        for (ForTable zxc : TableData) {
            if (!m2.containsKey(zxc.getNama())) {
                m2.put(zxc.getNama(), new SimpleStringProperty());
            }
        }
        
        FilteredList<ForTable> FL = new FilteredList<>(TableData, p -> true);
        NamaObat.textProperty().addListener((Observable, oldValue, newValue)-> {
            FL.setPredicate(ForTable -> {
                setJumlahPesanan();
              if (newValue == null || newValue.isEmpty()) {
                      return true;
                      
              }

              String lowerCaseFilter = newValue.toLowerCase();

              if (ForTable.getNama().toLowerCase().indexOf(lowerCaseFilter) != -1) {
                      return true; 
              }
              return false;
            });
        });
        SortedList<ForTable> sorted = new SortedList<>(FL);
        sorted.comparatorProperty().bind(Tabel.comparatorProperty());
        Tabel.setItems(sorted);
//        for (int i = 0; i < Tabel.getItems().size(); i++) {
//            if (!m.containsKey(Tabel.getItems().get(i).getNama())) {
//                m.put(Tabel.getItems().get(i).getNama(), "");
//            }
//        }
    }
    
    private ObservableList<ForTable> temp = FXCollections.observableArrayList();
    private Thread t = new Thread();

    @FXML
    private void TambahData(ActionEvent event) {
//        System.out.println("ssssssssssss     " + Tabel.getItems().get(Tabel.getSelectionModel().getSelectedIndex()).getNomorKontak());
//        if (m.containsKey(Tabel.getSelectionModel().getSelectedItem().getNama())) {
//            int y = Integer.parseInt(Tabel.getSelectionModel().getSelectedItem().getNomorKontak());
//            int x = Integer.parseInt(m.get(Tabel.getSelectionModel().getSelectedItem().getNama()));
//            m.put(Tabel.getSelectionModel().getSelectedItem().getNama(), ""+(x+y));
//        } else {
//            m.put(Tabel.getSelectionModel().getSelectedItem().getNama(), Tabel.getSelectionModel().getSelectedItem().getNomorKontak());
//        }
        temp.add(Tabel.getSelectionModel().getSelectedItem());
        wtfisthis();
        Tabel2.setItems(temp);
    }

    @FXML
    private void GotoProfil(MouseEvent event) {
        PleaseDontOpen pdo = new PleaseDontOpen();
        pdo.gotoProfil();
    }

    @FXML
    private void GotoHome(MouseEvent event) {
        t.stop();
        PleaseDontOpen pdo = new PleaseDontOpen();
        pdo.gotoScene(this.getClass().toString(), PaneAwal);
        
    }
    
    @FXML
    private void Goto(MouseEvent event) {
        t.stop();
        PleaseDontOpen pdo = new PleaseDontOpen();
        pdo.gotoScene(event.getSource().toString(), PaneAwal);
    }

    @FXML
    private void Keluar(MouseEvent event) {
        t.stop();
        PleaseDontOpen pdo = new PleaseDontOpen();
        pdo.gotoLoginForm(PaneAwal);
    }
    
    private void KeepOn() {
        t = new Thread() {
            public void run() {
                while(true) {
                    setDataTable();
                    try {
                        Thread.sleep(10000);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(LaporanAkunController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        };
        t.start();
    }
    
    class autoSave {
        private String x;
        
        public void setX(String value) {
            x = value;
        }
        
        public String getX() {
            return x;
        }
    }
}
