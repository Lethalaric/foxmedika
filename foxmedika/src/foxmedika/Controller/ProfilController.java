/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package foxmedika.Controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextInputDialog;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.PickResult;
import javafx.scene.layout.GridPane;
import javafx.stage.FileChooser;
import javax.swing.JOptionPane;
import org.controlsfx.control.ToggleSwitch;

/**
 * FXML Controller class
 *
 * @author Mukhtar
 */
public class ProfilController implements Initializable {
    @FXML
    private ImageView ImgProfil;
    @FXML
    private JFXTextField IDAkunField;
    @FXML
    private JFXTextField NamaField;
    @FXML
    private JFXTextField TTLField;
    @FXML
    private JFXTextField JKField;
    @FXML
    private JFXTextField KontakField;
    @FXML
    private JFXTextField AlamatField;
    @FXML
    private JFXTextField EmailField;
    @FXML
    private JFXTextField UsernameField;
    @FXML
    private JFXTextField JabatanField;
    @FXML
    private JFXPasswordField PasswordField;
    @FXML
    private JFXButton SimpanButton;
    @FXML
    private ToggleSwitch UbahButton;
    @FXML
    private Label Status;
    @FXML
    private GridPane PaneAwal;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        IDAkunField.setText("ping");
        NamaField.setText("ping");
        TTLField.setText("ping");
        JKField.setText("ping");
        KontakField.setText("ping");
        AlamatField.setText("ping");
        EmailField.setText("ping");
        UsernameField.setText("ping");
        PasswordField.setText("ping");
        JabatanField.setText("ping");
        setDisable();
        Ubah();
    }    
    
    public void setDisable() {
        IDAkunField.setDisable(true);
        NamaField.setDisable(true);
        TTLField.setDisable(true);
        JKField.setDisable(true);
        KontakField.setDisable(true);
        AlamatField.setDisable(true);
        EmailField.setDisable(true);
        UsernameField.setDisable(true);
        PasswordField.setDisable(true);
        JabatanField.setDisable(true);
    }

    @FXML
    private void Simpan(MouseEvent event) {
        JOptionPane.showMessageDialog(null, "Tersimpan");
    }

    private void Ubah() {
        UbahButton.selectedProperty().addListener(new ChangeListener<Boolean> () {

            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if (newValue == false) {
                    IDAkunField.setDisable(true);
                    NamaField.setDisable(true);
                    TTLField.setDisable(true);
                    JKField.setDisable(true);
                    KontakField.setDisable(true);
                    AlamatField.setDisable(true);
                    EmailField.setDisable(true);
                    UsernameField.setDisable(true);
                    PasswordField.setDisable(true);
                    JabatanField.setDisable(true);
                    Status.setText("Tidak Dapat Diubah");
                }
                else {
                    NamaField.setDisable(false);
                    TTLField.setDisable(false);
                    JKField.setDisable(false);
                    KontakField.setDisable(false);
                    AlamatField.setDisable(false);
                    EmailField.setDisable(false);
                    UsernameField.setDisable(false);
                    PasswordField.setDisable(false);
                    JabatanField.setDisable(false);
                    Status.setText("Dapat Diubah");
                }
            }
        }
        );
        
        
    }

    @FXML
    private void SetGambar(MouseEvent event) {
        if (event.getClickCount() >= 1) {
            FileChooser fc = new FileChooser();
            File f = fc.showOpenDialog(null);
            try {
                ImgProfil.setImage(new Image(f.toURI().toString()));
            } catch (NullPointerException e) {
                ImgProfil.setImage(null);
            }
        }
    }
    
}
