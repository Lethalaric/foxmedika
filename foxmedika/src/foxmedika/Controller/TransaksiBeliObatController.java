/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package foxmedika.Controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXCheckBox;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXSlider;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.controls.JFXTreeTableView;
import foxmedika.Foxmedika;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.geometry.Side;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TreeTableColumn;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.Callback;
import javax.swing.JOptionPane;
import org.apache.commons.lang.RandomStringUtils;
import org.controlsfx.control.HiddenSidesPane;
import org.controlsfx.control.Notifications;

/**
 * FXML Controller class
 *
 * @author Mukhtar
 */
public class TransaksiBeliObatController implements Initializable {
    @FXML
    private HiddenSidesPane pane;
    @FXML
    private ScrollPane SP;
    @FXML
    private TableView<ForTable> Tabel;
    @FXML
    private JFXButton toBarangKasir;
    @FXML
    private JFXButton toBeliObat;
    @FXML
    private JFXButton toInputResep;
    @FXML
    private JFXButton toPenjualanObat;
    @FXML
    private JFXButton toPreOrder;
    @FXML
    private JFXButton toRacikResep;
    @FXML
    private JFXButton Submit;
    @FXML
    private JFXButton Profil;
    @FXML
    private Label NamaKaryawan;
    @FXML
    private TableColumn<ForTable, String> IDObatRow;
    @FXML
    private TableColumn<ForTable, String> NamaObatRow;
    @FXML
    private TableColumn<ForTable, String> JenisObatRow;
    @FXML
    private TableColumn<ForTable, String> HargaRow;
    @FXML
    private TableColumn<ForTable, String> JumlahStokRow;
    @FXML
    private TableColumn<ForTable, String> BatasRow;
    @FXML
    private TableColumn<ForTable, String> JumlahPesanRow;
    @FXML
    private TableColumn<ForTable, String> CheckRow;
    @FXML
    private JFXTextField SearchNamaSupplier;
    
    private ObservableList<ForTable> TableData = FXCollections.observableArrayList();
    
    private ArrayList<String> dummyData = new ArrayList<>(Arrays.asList("Nunc vel est ullamcorper, egestas leo nec, scelerisque mauris. Sed aliquam, quam fringilla ultrices faucibus, turpis est dictum nulla, quis viverra orci diam ut ligula. Quisque consectetur felis sit amet pharetra ultrices. Donec congue sodales sodales. In scelerisque est nec lacus aliquam, quis vehicula sapien convallis. Maecenas non vulputate sapien, ut lacinia urna. Duis finibus odio aliquam lacus rhoncus feugiat. Vivamus condimentum, risus a dapibus finibus, est lorem ullamcorper lorem, vulputate vulputate arcu mi eget felis. Donec ultrices quam sapien, non dictum nibh facilisis ut. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Mauris ultrices facilisis orci, eu eleifend augue tincidunt non. Suspendisse elementum nisi sed massa volutpat, quis vehicula lectus pulvinar. Mauris scelerisque convallis nunc non faucibus. Aliquam tincidunt diam at augue bibendum ullamcorper eu quis quam. Nulla tincidunt ex blandit, laoreet purus a, vehicula sem. Quisque quis odio lorem. Etiam convallis scelerisque quam, vel lobortis libero efficitur at. Integer eget interdum dui, id sagittis est. Praesent ut egestas neque. Cras a molestie sem. Nullam non eleifend libero. Integer vitae purus vitae sapien molestie aliquam. Integer dapibus tortor ac metus pharetra molestie. Suspendisse mi magna, sodales in nibh nec, consequat placerat libero. Proin eget dolor nibh. Nulla quis maximus ligula. Aliquam sed mi dolor. Nulla eu lacus ligula.".split(" "))) ;
    @FXML
    private JFXDatePicker tanggal;
    @FXML
    private JFXButton Home;
    @FXML
    private JFXButton GetOut;
    @FXML
    private Pane PaneAwal;
   

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
//        binding();
        isiTabel();
        tanggal.setValue(LocalDate.now());
        KeepOn();
    }    

    @FXML
    private void handleMouseClicked(MouseEvent event) {
        if (pane.getPinnedSide() != null) {
            pane.setPinnedSide(null);
        } else {
            pane.setPinnedSide(Side.LEFT);
        }
    }
    
    private Thread t = new Thread();
    
    private void KeepOn() {
        t = new Thread() {
            public void run() {
                while(true) {
                    setDataTable();
                    try {
                        Thread.sleep(10000);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(TransaksiBeliObatController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        };
        t.start();
    }
    
//    private void binding() {
////        slideText.textProperty().bindBidirectional(new SimpleStringProperty(""+slide.getValue()));
//        slide.valueProperty().addListener(new ChangeListener(){
//            public void changed(ObservableValue arg0, Object arg1, Object arg2) {
//                slideText.textProperty().setValue(String.valueOf((int) slide.getValue()));
//            }
//        });
//        slideText.textProperty().addListener(new ChangeListener() {
//
//            @Override
//            public void changed(ObservableValue observable, Object oldValue, Object newValue) {
//                try {
//                    if (slideText.getText().equals("")) {
//                        slideText.setText("0");
//                    }
//                    slide.setValue(Integer.parseInt(slideText.getText()));
//                } catch (Exception e) {
//                    JOptionPane.showMessageDialog(null, "Hanya dapat mengisi angka", "Warning", 1);
//                    slide.setValue(0);
//                    slideText.setText("0");
//                }
//            }
//        });
//    }

    @FXML
    private void submit(ActionEvent event) {
        PleaseDontOpen pdo = new PleaseDontOpen();
        String[] randomString = new String[13];
        for (int i = 0; i < 13; i++) {
            randomString[i] = RandomStringUtils.randomAlphabetic(10);
        }
        String x = "mukhtar" + (Math.random()*10);
        String query = "insert into TableBaru (a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,a13) values ('" + randomString[0] + "','" + x + "','" +
                randomString[2] + "','" + randomString[3] + "','" + randomString[4] + "','" + randomString[5] + "','" + randomString[6] + "','" +
                randomString[7] + "','" + randomString[8] + "','" + randomString[9] + "','" + randomString[10] + "','" + randomString[11] + "','" +
                randomString[12] +"')";
        pdo.insertData(query);
        Notifications notif = Notifications.create();
        notif.title("Kirim ke Manajemen");
        notif.graphic(new Label("Data telah dimasukan"));
        notif.position(Pos.TOP_LEFT);
        notif.show();
    }

    private void AddObat(ActionEvent event) {
        Stage stage = new Stage();
        Parent root = null;
//        stage = (Stage) SP.getScene().getWindow();
//        stage.close();
        FXMLLoader loader = null;
        loader = new FXMLLoader(getClass().getResource("/foxmedika/FXML/TransaksiBeliObat_TambahObat.fxml"));
        try {
            root = loader.load();
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null, "Terjadi error, hubungi pihak adhivasindo.", "Error", 1);
        }
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }
    
    private void setCheck() {
          CheckRow.setCellFactory(new Callback<TableColumn<ForTable,String>,TableCell<ForTable,String>>(){        
            @Override
            public TableCell<ForTable,String> call(TableColumn<ForTable,String> param) {                
                TableCell<ForTable,String> cell = new TableCell<ForTable,String>(){
                    @Override
                    public void updateItem(String item, boolean empty) {                        
                        if(item!=null){                         
                            VBox vbox = new VBox();
                            vbox.setAlignment(Pos.CENTER);

//                            ImageView imageview = new ImageView();
//                            imageview.setFitHeight(50);
//                            imageview.setFitWidth(50);
//                            imageview.setImage(new Image(Foxmedika.class.getResource("Image").toString()+"/"+item)); 
                            
                            JFXCheckBox checkBox = new JFXCheckBox("");
                            checkBox.setPrefHeight(50);
                            checkBox.setPrefWidth(50);
//                            checkBox.selectedProperty().addListener(new ChangeListener<Boolean>() {
//
//                                @Override
//                                public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
//                                     if (newValue) {
//                                         
//                                     }
//                                }
//                            });

                            
//                            box.getChildren().addAll(imageview,vbox); 
//                            vbox.getChildren().add(imageview);
                            vbox.getChildren().add(checkBox);
                            //SETTING ALL THE GRAPHICS COMPONENT FOR CELL
                            setGraphic(vbox);
                        }
                    }
                };
//                System.out.println(cell.getIndex());               
                return cell;
            }
            
        });        
    }
    
    private void setJumlahPesanan() {
          JumlahPesanRow.setCellFactory(new Callback<TableColumn<ForTable,String>,TableCell<ForTable,String>>(){        
            @Override
            public TableCell<ForTable,String> call(TableColumn<ForTable,String> param) {                
                TableCell<ForTable,String> cell = new TableCell<ForTable,String>(){
                    @Override
                    public void updateItem(String item, boolean empty) {                        
                        if(item!=null){                         
                            VBox vbox = new VBox();
                            vbox.setAlignment(Pos.CENTER);

//                            ImageView imageview = new ImageView();
//                            imageview.setFitHeight(50);
//                            imageview.setFitWidth(50);
//                            imageview.setImage(new Image(Foxmedika.class.getResource("Image").toString()+"/"+item)); 
                            
                            JFXTextField textField = new JFXTextField();
                            textField.setPrefHeight(50);
                            textField.setPrefWidth(50);

                            
//                            box.getChildren().addAll(imageview,vbox); 
//                            vbox.getChildren().add(imageview);
                            vbox.getChildren().add(textField);
                            //SETTING ALL THE GRAPHICS COMPONENT FOR CELL
                            setGraphic(vbox);
                        }
                    }
                };
//                System.out.println(cell.getIndex());               
                return cell;
            }
            
        });        
      }
    
    private void isiTabel() {
        Tabel.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        IDObatRow.setCellValueFactory(new PropertyValueFactory<ForTable, String>("Nama"));
        NamaObatRow.setCellValueFactory(new PropertyValueFactory<ForTable, String>("Alamat"));
        JenisObatRow.setCellValueFactory(new PropertyValueFactory<ForTable, String>("Email"));
        HargaRow.setCellValueFactory(new PropertyValueFactory<ForTable, String>("Foto"));
        JumlahStokRow.setCellValueFactory(new PropertyValueFactory<ForTable, String>("JenisKelamin"));
        JumlahPesanRow.setCellValueFactory(new PropertyValueFactory<ForTable, String>("Jabatan"));
        BatasRow.setCellValueFactory(new PropertyValueFactory<ForTable, String>("NomorKontak"));
        CheckRow.setCellValueFactory(new PropertyValueFactory<ForTable, String>("LastLogin"));
//        setCheck();
//        setJumlahPesanan();
        setDataTable();
    }
    
    private void setDataTable() {
        TableData = FXCollections.observableArrayList();
        PleaseDontOpen pdo = new PleaseDontOpen();
        dummyData = pdo.RefreshTable("Select * from TableBaru", new String[]{"a1", "a2", "a3", "a4", "a5", "a6", "a7", "a8"});
        for (int i = 0; i < dummyData.size()-7; i+=8) {
            System.out.println(dummyData.get(i));
            ForTable temp = new ForTable(""+ dummyData.get(i), ""+ dummyData.get(i+1), ""+ dummyData.get(i+2), ""+ dummyData.get(i+3), ""+ dummyData.get(i+4), ""+ dummyData.get(i+5), ""+ dummyData.get(i+6), ""+ dummyData.get(i+7));
//            temp.setphoto("YY2.jpg");
            TableData.add(temp);
        }
        
        FilteredList<ForTable> FL = new FilteredList<>(TableData, p -> true);
        SearchNamaSupplier.textProperty().addListener((Observable, oldValue, newValue)-> {
            FL.setPredicate(ForTable -> {
                setCheck();
                setJumlahPesanan();
              if (newValue == null || newValue.isEmpty()) {
                      return true;
                      
              }

              String lowerCaseFilter = newValue.toLowerCase();

              if (ForTable.getNama().toLowerCase().indexOf(lowerCaseFilter) != -1) {
                      return true; 
              }
              return false;
            });
        });
        SortedList<ForTable> sorted = new SortedList<>(FL);
        sorted.comparatorProperty().bind(Tabel.comparatorProperty());
        Tabel.setItems(sorted);
      }

    @FXML
    private void GotoProfil(MouseEvent event) {
        PleaseDontOpen pdo = new PleaseDontOpen();
        pdo.gotoProfil();
    }

    @FXML
    private void GotoHome(MouseEvent event) {
        t.stop();
        PleaseDontOpen pdo = new PleaseDontOpen();
        pdo.gotoScene(this.getClass().toString(), PaneAwal);
        
    }
    
    @FXML
    private void Goto(MouseEvent event) {
        t.stop();
        PleaseDontOpen pdo = new PleaseDontOpen();
        pdo.gotoScene(event.getSource().toString(), PaneAwal);
    }

    @FXML
    private void Keluar(MouseEvent event) {
        t.stop();
        PleaseDontOpen pdo = new PleaseDontOpen();
        pdo.gotoLoginForm(PaneAwal);
    }
}
