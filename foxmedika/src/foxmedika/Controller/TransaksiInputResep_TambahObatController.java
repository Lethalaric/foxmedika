/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package foxmedika.Controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import org.controlsfx.control.Notifications;

/**
 * FXML Controller class
 *
 * @author Mukhtar
 */
public class TransaksiInputResep_TambahObatController implements Initializable {
    @FXML
    private JFXComboBox<String> IDObatBox;
    @FXML
    private Label NamaLabel;
    @FXML
    private Label JenisLabel;
    @FXML
    private Label HargaLabel;
    @FXML
    private JFXTextField JumlahField;
    @FXML
    private JFXComboBox<String> SupplierBox;
    @FXML
    private JFXButton TambahObat;
    @FXML
    private AnchorPane PaneAwal;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        loadIDObat();
        loadSupplier();
    }    

    @FXML
    private void addObat(ActionEvent event) {
        PleaseDontOpen pdo = new PleaseDontOpen();
        pdo.insertData("insert into TableBaru (a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,a13) values ('" + IDObatBox.getSelectionModel().getSelectedItem() +  "','" +
                NamaLabel.getText() + "','" + JenisLabel.getText() + "','" + HargaLabel.getText() + "','" + JumlahField.getText() + "','" + SupplierBox.getSelectionModel().getSelectedItem() + "','" +
                "apa aja" + "','" + "yang penting" + "','" + "masuk ke" + "','" + "dalam database" + "','" + "dan berjalan" + "','" +
                "sangat lancar" + "','" + "aminnnnnn" +"')");
        Notifications notif = Notifications.create();
        notif.title("Kirim ke Manajemen");
        notif.graphic(new Label("Data telah ditolak"));
        notif.position(Pos.TOP_LEFT);
        notif.show();
        Stage stage = (Stage) SupplierBox.getScene().getWindow();
        stage.close();
    }
    
    private void loadSupplier() {
        ObservableList<String> data = FXCollections.observableArrayList("si Jono", "si Jona", "si Jonu", "si Jone", "si Joni");
        if (SupplierBox != null) {
            SupplierBox.getItems().clear();
        }
        SupplierBox.getItems().addAll(data);
    }
    
    private void loadIDObat() {
        ObservableList<String> data = FXCollections.observableArrayList("Obat 001", "Obat 002", "Obat 003", "Obat 004", "Obat 005");
        if (IDObatBox != null) {
            IDObatBox.getItems().clear();
        }
        IDObatBox.getItems().addAll(data);
    }
}
