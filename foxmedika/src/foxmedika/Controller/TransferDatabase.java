/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package foxmedika.Controller;

import java.sql.*;
import java.util.ArrayList;

/**
 *
 * @author Mukhtar
 */
public class TransferDatabase {
    
//    private String DBURL = "jdbc:derby://localhost:1527/myDB;create=true;user=me;password=mine";
    private String DBURL = "jdbc:derby://192.168.150.69:1527/myDB;create=true";
//    private String DBURL = "jdbc:derby:myDB;create=true;user=me;password=mine";
//    private String tableName = "ForTable";
    private Connection conn = null;
    private Statement stat = null;
    
    public void CreateConnection() {
        try {
//            Class.forName("org.apache.derby.jdbc.EmbeddedDriver").newInstance();
            Class.forName("org.apache.derby.jdbc.ClientDriver").newInstance();
            conn = DriverManager.getConnection(DBURL);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
//    public void CreateConnection(String URLValue) {
//        try {
//            String url = "jdbc:derby://"+ URLValue +":1527/myDB;";
////            Class.forName("org.apache.derby.jdbc.EmbeddedDriver").newInstance();
//            Class.forName("org.apache.derby.jdbc.ClientDriver").newInstance();
//            conn = DriverManager.getConnection(url);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
    
    public void CreateConnection(String value) throws Exception {
        String DBURL = "jdbc:h2:tcp://"+value+"/~/myDB";
//        try {
//            Class.forName("org.apache.derby.jdbc.EmbeddedDriver").newInstance();
            Class.forName("org.h2.Driver");
            conn = DriverManager.getConnection(DBURL);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
    }
    
    public void CreateTable() {
        try {
            stat = conn.createStatement();
            stat.executeUpdate(
            "CREATE TABLE ForTable (nomor INT, nama VARCHAR(20),"
            + " TTL VARCHAR(20))");
            System.out.println("Table created.");
            stat.close();        

            conn.close();
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void InsertData(String sql) {
        try {
            stat = conn.createStatement();
            stat.execute(sql);
            stat.close();
            conn.close();
        } catch (Exception e) {
            e.printStackTrace();
        } 
    }
    
    ArrayList<String> data = new ArrayList<>();
    
    public void SelectData(String sql) {
        data = new ArrayList<>();
        try {
            stat = conn.createStatement();
            ResultSet rs = stat.executeQuery(sql);
            while (rs.next()) {
                data.add(""+rs.getString(2));
//                data.add(""+rs.getString("nama"));
//                data.add(""+rs.getString("TTL"));
            }
            rs.close();
            stat.close();
            conn.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void SelectData(String sql, String[] parameter) {
        try {
            stat = conn.createStatement();
            ResultSet rs = stat.executeQuery(sql);
            for (String s : parameter) {
                System.out.println(s);
            }
            while (rs.next()) {
                for (String s : parameter) {
                    data.add(""+rs.getString(s));
                }
//                data.add(""+rs.getString("a1"));
//                data.add(""+rs.getString("a2"));
//                data.add(""+rs.getString("a3"));
//                data.add(""+rs.getString("a4"));
//                data.add(""+rs.getString("a5"));
//                data.add(""+rs.getString("a6"));
//                data.add(""+rs.getString("a7"));
//                data.add(""+rs.getString("a8"));
//                data.add(""+rs.getString("a9"));
//                data.add(""+rs.getString("a10"));
//                data.add(""+rs.getString("a11"));
//                data.add(""+rs.getString("a12"));
//                data.add(""+rs.getString("a13"));
            }
            rs.close();
            stat.close();
            conn.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
