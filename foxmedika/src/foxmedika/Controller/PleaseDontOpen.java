/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package foxmedika.Controller;

import com.jfoenix.controls.JFXButton;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.FadeTransition;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import javax.swing.JOptionPane;
import org.controlsfx.control.Notifications;

/**
 *
 * @author Mukhtar
 */
public class PleaseDontOpen {
    
    //database operation
    
    public ArrayList<String> RefreshTable(String query, String[] data) {
        ArrayList<String> dummyData = new ArrayList<>();
        String ipaddress = "172.97.4.196";
        YourSalvation ys = new YourSalvation();
        ipaddress = ys.getPreference();
        System.out.println(ipaddress);
        TransferDatabase td = new TransferDatabase();
        try {
            td.CreateConnection(ipaddress);
        } catch (Exception ex) {
            Logger.getLogger(PleaseDontOpen.class.getName()).log(Level.SEVERE, null, ex);
        }
//        td.SelectData("Select * from TableBaru", new String[]{"a1", "a2", "a3", "a4", "a5", "a6", "a7", "a8", "a9", "a10", "a11", "a12", "a13"});
        td.SelectData(query, data);
        if ((dummyData.size() != td.data.size()) || dummyData.isEmpty()) {
            dummyData = td.data;
    //            Notifications notif = Notifications.create();
    //            notif.title("Kirim ke Manajemen");
    //            notif.graphic(new Label("Data telah di kirim ke manajer"));
    //            notif.position(Pos.TOP_LEFT);
    //            notif.show();
            Platform.runLater(()-> {
                Notifications notif = Notifications.create();
                notif.title("Kirim ke Manajemen");
                notif.graphic(new Label("Data telah di kirim ke manajer"));
                notif.position(Pos.TOP_LEFT);
                notif.show();
            });
        }
        
        return dummyData;
    }
    
    public ArrayList<String> RefreshTable(String query, String[] data, String message) {
        ArrayList<String> dummyData = new ArrayList<>();
        String ipaddress = "172.97.4.196";
        YourSalvation ys = new YourSalvation();
        ipaddress = ys.getPreference();
        System.out.println(ipaddress);
        TransferDatabase td = new TransferDatabase();
        try {
            td.CreateConnection(ipaddress);
        } catch (Exception ex) {
            Logger.getLogger(PleaseDontOpen.class.getName()).log(Level.SEVERE, null, ex);
        }
//        td.SelectData("Select * from TableBaru", new String[]{"a1", "a2", "a3", "a4", "a5", "a6", "a7", "a8", "a9", "a10", "a11", "a12", "a13"});
        td.SelectData(query, data);
        if ((dummyData.size() != td.data.size()) || dummyData.isEmpty()) {
            dummyData = td.data;
    //            Notifications notif = Notifications.create();
    //            notif.title("Kirim ke Manajemen");
    //            notif.graphic(new Label("Data telah di kirim ke manajer"));
    //            notif.position(Pos.TOP_LEFT);
    //            notif.show();
            Platform.runLater(()-> {
                Notifications notif = Notifications.create();
                notif.title("Kirim ke Manajemen");
                notif.graphic(new Label(message));
                notif.position(Pos.TOP_LEFT);
                notif.show();
            });
        }
        
        return dummyData;
    }
    
    public void insertData(String query) {
        YourSalvation ys = new YourSalvation();
        String ipaddress = ys.getPreference();
        TransferDatabase td = new TransferDatabase();
        try {
            td.CreateConnection(ipaddress);
        } catch (Exception ex) {
            Logger.getLogger(PleaseDontOpen.class.getName()).log(Level.SEVERE, null, ex);
        }
        td.InsertData(query);
    }
    
    
    
    
    
    
    //change scene
    
    public void gotoScene(String event, Parent value2) {
        Platform.runLater(new Runnable() {

            @Override
            public void run() {
                gotoScene2(event, value2);
            }
        });
    }
    
    private void gotoScene2(String event, Parent value2) {
        Stage stage = new Stage();
        Parent root = null;
        stage = (Stage) value2.getScene().getWindow();
        FXMLLoader loader = null;
//        FadeTransition ft1 = new FadeTransition(Duration.millis(1000), value2);
//        ft1.setFromValue(1);
//        ft1.setToValue(0);
//        ft1.setCycleCount(1);
//        ft1.play();
        stage.close();
        
        root = null;
        loader = null;
        System.out.println(event);
        String[] s = event.split(", ");
        System.out.println(s[0]);
        //Manager
        if (s[0].equals("JFXButton[id=toOpname")) {
            loader = new FXMLLoader(getClass().getResource("/foxmedika/FXML/LaporanOpname.fxml"));
        }
        else if (s[0].equals("JFXButton[id=toJualBeli")) {
            loader = new FXMLLoader(getClass().getResource("/foxmedika/FXML/LaporanJualBeli.fxml"));
        }
        else if (s[0].equals("JFXButton[id=toTransaksiBeliObat")) {
            loader = new FXMLLoader(getClass().getResource("/foxmedika/FXML/TransaksiBeliObatManager.fxml"));
        }
        else if (s[0].equals("JFXButton[id=toLabaRugi")) {
            loader = new FXMLLoader(getClass().getResource("/foxmedika/FXML/LaporanLabaRugi.fxml"));
        }
        else if (s[0].equals("JFXButton[id=toAkun")) {
            loader = new FXMLLoader(getClass().getResource("/foxmedika/FXML/LaporanAkun.fxml"));
        }
        else if (s[0].equals("JFXButton[id=toStatistik")) {
            loader = new FXMLLoader(getClass().getResource("/foxmedika/FXML/LaporanStatistik.fxml"));
        }
        else if (s[0].equals("JFXButton[id=toNeraca")) {
            JOptionPane.showMessageDialog(null, "under construction");
        }
        //Kasir
        else if (s[0].equals("JFXButton[id=toBarangKasir")) {
            loader = new FXMLLoader(getClass().getResource("/foxmedika/FXML/TransaksiBarangToKasir.fxml"));
        }
        else if (s[0].equals("JFXButton[id=toBeliObat")) {
            loader = new FXMLLoader(getClass().getResource("/foxmedika/FXML/TransaksiBeliObat.fxml"));
        }
        else if (s[0].equals("JFXButton[id=toInputResep")) {
            loader = new FXMLLoader(getClass().getResource("/foxmedika/FXML/TransaksiInputResep.fxml"));
        }
        else if (s[0].equals("JFXButton[id=toPenjualanObat")) {
            loader = new FXMLLoader(getClass().getResource("/foxmedika/FXML/TransaksiPenjualanObat.fxml"));
        }
        else if (s[0].equals("JFXButton[id=toPreOrder")) {
            loader = new FXMLLoader(getClass().getResource("/foxmedika/FXML/TransaksiPO.fxml"));
        }
        else if (s[0].equals("JFXButton[id=toRacikResep")) {
            loader = new FXMLLoader(getClass().getResource("/foxmedika/FXML/TransaksiRacikResep.fxml"));
        }
        else if (event.contains("Laporan") || event.contains("Manager")) {
            loader = new FXMLLoader(getClass().getResource("/foxmedika/FXML/LaporanOpname.fxml"));
        }
        else if (event.contains("Transaksi") && !event.contains("Manager")) {
            loader = new FXMLLoader(getClass().getResource("/foxmedika/FXML/TransaksiBarangToKasir.fxml"));
        }
        try {
            root = loader.load();
            
//            untuk by-pass value
//            MainController mc = loader.<MainController>getController();
//            mc.setStatus(status);

        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null, "Terjadi error, hubungi pihak adhivasindo.", "Error", 1);
        }
        
        FadeTransition ft2 = new FadeTransition(Duration.millis(1000), root);
        ft2.setFromValue(0);
        ft2.setToValue(1);
        ft2.setCycleCount(1);
        ft2.play();
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }
    
    public void gotoProfil() {
        Platform.runLater(new Runnable() {

            @Override
            public void run() {
                gotoProfil2();
            }
        });
    }
    
    private void gotoProfil2() {
        Stage stage = new Stage();
        Parent root = null;
        FXMLLoader loader = null;
        loader = new FXMLLoader(getClass().getResource("/foxmedika/FXML/Profil.fxml"));
        try {
            root = loader.load();
            
//            untuk by-pass value
//            MainController mc = loader.<MainController>getController();
//            mc.setStatus(status);

        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null, "Terjadi error, hubungi pihak adhivasindo.", "Error", 1);
        }
        
        FadeTransition ft2 = new FadeTransition(Duration.millis(1000), root);
        ft2.setFromValue(0);
        ft2.setToValue(1);
        ft2.setCycleCount(1);
        ft2.play();
        Scene scene = new Scene(root);
        stage.setScene(scene);
//        stage.initModality(Modality.APPLICATION_MODAL);
        stage.show();
    }
    
    public void gotoLoginForm(Parent value2) {
        Platform.runLater(new Runnable() {

            @Override
            public void run() {
                gotoLoginForm2(value2);
            }
        });
    }
    
//    public void gotoLoginForm(Label value2) {
//        Platform.runLater(new Runnable() {
//
//            @Override
//            public void run() {
//                gotoLoginForm2(value2);
//            }
//        });
//    }
    
    private void gotoLoginForm2(Parent value2) {
        Stage stage = new Stage();
        Parent root = null;
        stage = (Stage) value2.getScene().getWindow();
        FXMLLoader loader = null;
//        FadeTransition ft1 = new FadeTransition(Duration.millis(1000), value2);
//        ft1.setFromValue(1);
//        ft1.setToValue(0);
//        ft1.setCycleCount(1);
//        ft1.play();
        stage.close();
        stage = new Stage();
        stage = new Stage(StageStyle.UNDECORATED);
        stage.resizableProperty().setValue(Boolean.FALSE);
        stage.setTitle("Fox Medika");
        stage.getIcons().add(new Image("file:src/FXML/icon/icon.png"));
        loader = new FXMLLoader(getClass().getResource("/foxmedika/FXML/LoginForm.fxml"));
        try {
            root = loader.load();
            
//            untuk by-pass value
//            MainController mc = loader.<MainController>getController();
//            mc.setStatus(status);

        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null, "Terjadi error, hubungi pihak adhivasindo.", "Error", 1);
        }
                  
        FadeTransition ft2 = new FadeTransition(Duration.millis(1000), root);
        ft2.setFromValue(0);
        ft2.setToValue(1);
        ft2.setCycleCount(1);
        ft2.play();
        Scene scene = new Scene(root);
        scene.getStylesheets().add(getClass().getResource("/foxmedika/FXML/style/login.css").toExternalForm());
        stage.setScene(scene);
        stage.show();
    }
    
//    public void gotoLoginForm2(Label value2, ) {
//        Stage stage = new Stage();
//        Parent root = null;
//        stage = (Stage) value2.getScene().getWindow();
//        FXMLLoader loader = null;
//        loader = new FXMLLoader(getClass().getResource(value));
//        try {
//            root = loader.load();
//        } catch (IOException ex) {
//            Logger.getLogger(PleaseDontOpen.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        FadeTransition ft1 = new FadeTransition(Duration.millis(1000), root);
//        ft1.setFromValue(1);
//        ft1.setToValue(0);
//        ft1.setCycleCount(1);
//        ft1.play();
//        stage.close();
//        root = null;
//        loader = null;
//        stage = new Stage();
//        stage = new Stage(StageStyle.UNDECORATED);
//        stage.resizableProperty().setValue(Boolean.FALSE);
//        stage.setTitle("Fox Medika");
//        stage.getIcons().add(new Image("file:src/FXML/icon/icon.png"));
//        loader = new FXMLLoader(getClass().getResource("/foxmedika/FXML/LoginForm.fxml"));
//        try {
//            root = loader.load();
//            
////            untuk by-pass value
////            MainController mc = loader.<MainController>getController();
////            mc.setStatus(status);
//
//        } catch (IOException ex) {
//            JOptionPane.showMessageDialog(null, "Terjadi error, hubungi pihak adhivasindo.", "Error", 1);
//        }
//                  
//        FadeTransition ft2 = new FadeTransition(Duration.millis(1000), root);
//        ft2.setFromValue(0);
//        ft2.setToValue(1);
//        ft2.setCycleCount(1);
//        ft2.play();
//        Scene scene = new Scene(root);
//        scene.getStylesheets().add(getClass().getResource("/foxmedika/FXML/style/login.css").toExternalForm());
//        stage.setScene(scene);
//        stage.show();
//    }
}
