/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package foxmedika.Controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXSlider;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.controls.JFXTreeTableView;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Side;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TreeTableColumn;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javax.swing.JOptionPane;
import org.controlsfx.control.HiddenSidesPane;

/**
 * FXML Controller class
 *
 * @author Mukhtar
 */
public class TransaksiInputResepController implements Initializable {
    @FXML
    private HiddenSidesPane pane;
    @FXML
    private ScrollPane SP;
    @FXML
    private TableView<ForTable> Tabel;
    @FXML
    private JFXButton toBarangKasir;
    @FXML
    private JFXButton toBeliObat;
    @FXML
    private JFXButton toInputResep;
    @FXML
    private JFXButton toPenjualanObat;
    @FXML
    private JFXButton toPreOrder;
    @FXML
    private JFXButton toRacikResep;
    @FXML
    private JFXButton TambahObat;
    @FXML
    private Label TotalHarga;
    @FXML
    private JFXTextField NamaRacikan;
    @FXML
    private JFXButton SubmitResep;
    @FXML
    private TableColumn<ForTable, String> IDObat;
    @FXML
    private TableColumn<ForTable, String> NamaObat;
    @FXML
    private TableColumn<ForTable, String> HargaSatuan;
    @FXML
    private TableColumn<ForTable, String> JumlahObat;
    @FXML
    private TableColumn<ForTable, String> JumlahHarga;
    @FXML
    private JFXButton Profil;
    @FXML
    private JFXButton Home;
    @FXML
    private JFXButton GetOut;
    private Thread t = new Thread();
    @FXML
    private Pane PaneAwal;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        isiTabel();
        setDataTable();
        keepOn();
    }    

    private void isiTabel() {
        Tabel.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        IDObat.setCellValueFactory(new PropertyValueFactory<ForTable, String>("IDAkun"));
        NamaObat.setCellValueFactory(new PropertyValueFactory<ForTable, String>("Alamat"));
        JumlahHarga.setCellValueFactory(new PropertyValueFactory<ForTable, String>("Email"));
        HargaSatuan.setCellValueFactory(new PropertyValueFactory<ForTable, String>("Foto"));
        JumlahObat.setCellValueFactory(new PropertyValueFactory<ForTable, String>("JenisKelamin"));
    }
    
    private void keepOn() {
        t = new Thread() {
            public void run() {
                while(true) {
                    setDataTable();
                    try {
                        Thread.sleep(10000);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(LaporanAkunController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        };
        t.start();
    }
    
    private ObservableList<ForTable> TableData = FXCollections.observableArrayList();
    
    private void setDataTable() {
        TableData = FXCollections.observableArrayList();
        Tabel.getItems().clear();
        PleaseDontOpen pdo = new PleaseDontOpen();
//        td.SelectData("Select * from TableBaru", new String[]{"a1", "a2", "a3", "a4", "a5", "a6", "a7"});
        ArrayList<String> dummyData2 = pdo.RefreshTable("Select * from TableBaru", new String[]{"a1", "a2", "a3", "a4", "a5", "a6"});
        for (int i=0; i < dummyData2.size()-7; i+=7) {
            ForTable temp = new ForTable(""+dummyData2.get(i), ""+dummyData2.get(i+1), ""+dummyData2.get(i+2), ""+dummyData2.get(i+3), ""+dummyData2.get(i+4), ""+dummyData2.get(i+5));
            TableData.add(temp);
        }
        Tabel.getItems().addAll(TableData);
    }
    
    @FXML
    private void handleMouseClicked(MouseEvent event) {
        if (pane.getPinnedSide() != null) {
            pane.setPinnedSide(null);
        } else {
            pane.setPinnedSide(Side.LEFT);
        }
    }

    @FXML
    private void TambahObat(ActionEvent event) {
        Stage stage = new Stage();
        Parent root = null;
//        stage = (Stage) SP.getScene().getWindow();
//        stage.close();
        FXMLLoader loader = null;
        loader = new FXMLLoader(getClass().getResource("/foxmedika/FXML/TransaksiInputResep_TambahObat.fxml"));
        try {
            root = loader.load();
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null, "Terjadi error, hubungi pihak adhivasindo.", "Error", 1);
        }
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    @FXML
    private void GotoProfil(MouseEvent event) {
        PleaseDontOpen pdo = new PleaseDontOpen();
        pdo.gotoProfil();
    }

    @FXML
    private void GotoHome(MouseEvent event) {
        t.stop();
        PleaseDontOpen pdo = new PleaseDontOpen();
        pdo.gotoScene(this.getClass().toString(), PaneAwal);
        
    }
    
    @FXML
    private void Goto(MouseEvent event) {
        t.stop();
        PleaseDontOpen pdo = new PleaseDontOpen();
        pdo.gotoScene(event.getSource().toString(), PaneAwal);
    }

    @FXML
    private void Keluar(MouseEvent event) {
        t.stop();
        PleaseDontOpen pdo = new PleaseDontOpen();
        pdo.gotoLoginForm(PaneAwal);
    }
    
}
