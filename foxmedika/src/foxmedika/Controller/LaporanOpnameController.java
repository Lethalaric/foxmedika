/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package foxmedika.Controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;           
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Side;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import org.controlsfx.control.HiddenSidesPane;

/**
 * FXML Controller class
 *
 * @author Mukhtar
 */
public class LaporanOpnameController implements Initializable {
    @FXML
    private HiddenSidesPane pane;
    @FXML
    private ScrollPane SP;
    @FXML
    private JFXButton toOpname;
    @FXML
    private JFXButton toJualBeli;
    @FXML
    private JFXButton toLabaRugi;
    @FXML
    private JFXButton toStatistik;
    @FXML
    private JFXButton toAkun;
    @FXML
    private JFXButton toNeraca;
    @FXML
    private JFXButton Profil;
    @FXML
    private JFXButton toTransaksiBeliObat;
    @FXML
    private TableView<ForTable> Tabel;
    @FXML
    private TableColumn<ForTable, String> IDObatColumn;
    @FXML
    private TableColumn<ForTable, String> TanggalColumn;
    @FXML
    private TableColumn<ForTable, String> NamaColumn;
    @FXML
    private TableColumn<ForTable, String> HargaColumn;
    @FXML
    private TableColumn<ForTable, String> JumlahMasukColumn;
    @FXML
    private TableColumn<ForTable, String> JumlahKeluarColumn;
    @FXML
    private TableColumn<ForTable, String> JumlahTotalColumn;
    @FXML
    private JFXButton HomeButton;
    @FXML
    private JFXButton GetOut;
    @FXML
    private Pane PaneAwal;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        Tabel.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        IDObatColumn.setCellValueFactory(new PropertyValueFactory<ForTable, String>("IDAkun"));
        NamaColumn.setCellValueFactory(new PropertyValueFactory<ForTable, String>("Alamat"));
        TanggalColumn.setCellValueFactory(new PropertyValueFactory<ForTable, String>("Email"));
        HargaColumn.setCellValueFactory(new PropertyValueFactory<ForTable, String>("Foto"));
        JumlahKeluarColumn.setCellValueFactory(new PropertyValueFactory<ForTable, String>("JenisKelamin"));
        JumlahMasukColumn.setCellValueFactory(new PropertyValueFactory<ForTable, String>("Jabatan"));
        JumlahTotalColumn.setCellValueFactory(new PropertyValueFactory<ForTable, String>("NomorKontak"));
        isiPilihan();
        setTableGudang();
        keepOn();
    }    

    @FXML
    private void handleMouseClicked(MouseEvent event) {
        if (pane.getPinnedSide() != null) {
            pane.setPinnedSide(null);
        } else {
            pane.setPinnedSide(Side.LEFT);
        }
    }

    @FXML
    private void Goto(MouseEvent event) {
//        Stage stage = new Stage();
//        Parent root = null;
//        stage = (Stage) toAkun.getScene().getWindow();
//        stage.close();
//        FXMLLoader loader = null;
//        if (event.getSource() == toOpname) {
//            loader = new FXMLLoader(getClass().getResource("/foxmedika/FXML/LaporanOpname.fxml"));
//        }
//        else if (event.getSource() == toJualBeli) {
//            loader = new FXMLLoader(getClass().getResource("/foxmedika/FXML/LaporanJualBeli.fxml"));
//        }
//        else if (event.getSource() == toTransaksiBeliObat) {
//            loader = new FXMLLoader(getClass().getResource("/foxmedika/FXML/TransaksiBeliObatManager.fxml"));
//        }
//        else if (event.getSource() == toLabaRugi) {
//            loader = new FXMLLoader(getClass().getResource("/foxmedika/FXML/LaporanLabaRugi.fxml"));
//        }
//        else if (event.getSource() == toAkun) {
//            loader = new FXMLLoader(getClass().getResource("/foxmedika/FXML/LaporanAkun.fxml"));
//        }
//        else if (event.getSource() == toStatistik) {
//            loader = new FXMLLoader(getClass().getResource("/foxmedika/FXML/LaporanStatistik.fxml"));
//        }
//        else if (event.getSource() == toNeraca) {
//            JOptionPane.showMessageDialog(null, "under construction");
//        }
//        try {
//            root = loader.load();
//            
////            untuk by-pass value
////            MainController mc = loader.<MainController>getController();
////            mc.setStatus(status);
//
//        } catch (IOException ex) {
//            JOptionPane.showMessageDialog(null, "Terjadi error, hubungi pihak adhivasindo.", "Error", 1);
//        }
//        Scene scene = new Scene(root);
//        stage.setScene(scene);
//        stage.show();
        t.stop();
        PleaseDontOpen pdo = new PleaseDontOpen();
        pdo.gotoScene(event.getSource().toString(), PaneAwal);
    }
    
    @FXML
    private JFXComboBox<String> ComboBox;
    
    private void isiPilihan() {
        ObservableList<String> data = FXCollections.observableArrayList("Laporan Gudang","Laporan Kasir");
        if (ComboBox != null) {
            ComboBox.getItems().clear();
        }
        ComboBox.getItems().addAll(data);
    }
    
    private ObservableList<ForTable> TableData = FXCollections.observableArrayList();
    
    private void setTableGudang() {
        TableData = FXCollections.observableArrayList();
        Tabel.getItems().clear();
        PleaseDontOpen pdo = new PleaseDontOpen();
//        td.SelectData("Select * from TableBaru", new String[]{"a1", "a2", "a3", "a4", "a5", "a6", "a7"});
        ArrayList<String> dummyData2 = pdo.RefreshTable("Select * from TableBaru", new String[]{"a1", "a2", "a3", "a4", "a5", "a6", "a7"});
        for (int i=0; i < dummyData2.size()-7; i+=7) {
            ForTable temp = new ForTable(""+dummyData2.get(i), ""+dummyData2.get(i+1), ""+dummyData2.get(i+2), ""+dummyData2.get(i+3), ""+dummyData2.get(i+4), ""+dummyData2.get(i+5), ""+dummyData2.get(i+6));
            TableData.add(temp);
        }
        Tabel.getItems().addAll(TableData);
    }
    
    private void setTableKasir() {
        TableData = FXCollections.observableArrayList();
        Tabel.getItems().clear();
        PleaseDontOpen pdo = new PleaseDontOpen();
//        td.SelectData("Select * from TableBaru", new String[]{"a1", "a2", "a3", "a4", "a5", "a6", "a7"});
        ArrayList<String> dummyData2 = pdo.RefreshTable("Select * from TableBaru", new String[]{"a1", "a2", "a3", "a4", "a5", "a6", "a7"}, "opname");
        for (int i=0; i < dummyData2.size()-7; i+=7) {
            ForTable temp = new ForTable(""+dummyData2.get(i), ""+dummyData2.get(i+1), ""+dummyData2.get(i+2), ""+dummyData2.get(i+3), ""+dummyData2.get(i+4), ""+dummyData2.get(i+5), ""+dummyData2.get(i+6));
            TableData.add(temp);
        }
        Tabel.getItems().addAll(TableData);
    }

    @FXML
    private void GotoProfil(MouseEvent event) {
//        Stage stage = new Stage();
//        Parent root = null;
//        FXMLLoader loader = null;
//        loader = new FXMLLoader(getClass().getResource("/foxmedika/FXML/Profil.fxml"));
//        try {
//            root = loader.load();
//            
////            untuk by-pass value
////            MainController mc = loader.<MainController>getController();
////            mc.setStatus(status);
//
//        } catch (IOException ex) {
//            JOptionPane.showMessageDialog(null, "Terjadi error, hubungi pihak adhivasindo.", "Error", 1);
//        }
//        Scene scene = new Scene(root);
//        stage.setScene(scene);
//        stage.show();
        PleaseDontOpen pdo = new PleaseDontOpen();
        pdo.gotoProfil();
    }

    @FXML
    private void Pilih(ActionEvent event) {
        if (ComboBox.getSelectionModel().getSelectedIndex() == 0) {
            setTableGudang();
        } else if(ComboBox.getSelectionModel().getSelectedIndex() == 1) {
            setTableKasir();
        }
    }
    
    private Thread t = new Thread();
    
    private void keepOn() {
        t = new Thread() {
            public void run() {
                while(true) {
                    if (ComboBox.getSelectionModel().getSelectedIndex() == 0) {
                        setTableGudang();
                    }
                    if (ComboBox.getSelectionModel().getSelectedIndex() == 1) {
                        setTableKasir();
                    }
                    try {
                        Thread.sleep(10000);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(LaporanAkunController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        };
        t.start();
    }

    @FXML
    private void GotoHome(MouseEvent event) {
        t.stop();
        PleaseDontOpen pdo = new PleaseDontOpen();
        pdo.gotoScene(this.getClass().toString(), PaneAwal);
    }

    @FXML
    private void Keluar(MouseEvent event) {
        t.stop();
        PleaseDontOpen pdo = new PleaseDontOpen();
        pdo.gotoLoginForm(PaneAwal);
    }
}
