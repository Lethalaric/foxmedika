package foxmedika;

import javafx.animation.FadeTransition;
import javafx.application.Application;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.collections.*;
import javafx.concurrent.*;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.*;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.stage.*;
import javafx.util.Duration;

/**
 * Example of displaying a splash page for a standalone JavaFX application
 */
public class Login extends Application {
    public static final String SPLASH_IMAGE =
            "file:src/fxml/images/splash.png";
    private Pane splashLayout;
    private Label progressText;
    private Stage mainStage;

    public static void main(String[] args) throws Exception {
        launch(args);
    }

    @Override
    public void init() {
        ImageView splash = new ImageView(new Image(
                SPLASH_IMAGE
        ));
        progressText = new Label("			Getting ready");
        splashLayout = new VBox();
        splashLayout.setStyle("-fx-background-color: transparent;");
        splashLayout.getChildren().addAll(splash, progressText);
        progressText.setStyle("-fx-alignment : center; -fx-font-size: 20;");

        
    }

    @Override
    public void start(final Stage initStage) throws Exception {
        final Task<ObservableList<String>> friendTask = new Task<ObservableList<String>>() {
            @Override
            protected ObservableList<String> call() throws InterruptedException {
                ObservableList<String> foundFriends =
                        FXCollections.<String>observableArrayList();
                ObservableList<String> ready =
                        FXCollections.observableArrayList(
                                " . ", " . . ", " . . . ", " . . . . ", " . . . . . ",
                                " . . . . . . ", " . . . . . . . ", " . . . . . . . . ", 
                                " . . . . . . . . . ", " . . . . . . . . . . "
                        );

                updateMessage("			Getting ready ");
                for (int i = 0; i < ready.size(); i++) {
                    Thread.sleep(400);
                    updateProgress(i + 1, ready.size());
                    String next = ready.get(i);
                    foundFriends.add(next);
                    updateMessage("			Getting ready " + next);
                }
                Thread.sleep(400);
                updateMessage("Ready.");

                return foundFriends;
            }
        };

        showSplash(
                initStage,
                friendTask,
                () -> showMainStage(friendTask.valueProperty())
        );
        new Thread(friendTask).start();
    }

    private void showMainStage(
            ReadOnlyObjectProperty<ObservableList<String>> friends
    ) {
        mainStage = new Stage(StageStyle.UNDECORATED);
        mainStage.resizableProperty().setValue(Boolean.FALSE);
        mainStage.setTitle("Fox Medika");
        mainStage.getIcons().add(new Image("file:src/FXML/icon/icon.png"));
        try {
            Parent root = FXMLLoader.load(getClass().getResource("FXML/LoginForm.fxml"));
            Scene scene = new Scene(root);
            scene.getStylesheets().add(getClass().getResource("FXML/style/login.css").toExternalForm());
            mainStage.setScene(scene);
            mainStage.show();

        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    private void showSplash(
            final Stage initStage,
            Task<?> task,
            InitCompletionHandler initCompletionHandler
    ) {
        progressText.textProperty().bind(task.messageProperty());
        task.stateProperty().addListener((observableValue, oldState, newState) -> {
            if (newState == Worker.State.SUCCEEDED) {
                initStage.toFront();
                FadeTransition fadeSplash = new FadeTransition(Duration.seconds(1.2), splashLayout);
                fadeSplash.setFromValue(1.0);
                fadeSplash.setToValue(0.0);
                fadeSplash.setOnFinished(actionEvent -> initStage.hide());
                fadeSplash.play();

                initCompletionHandler.complete();
            } // todo add code to gracefully handle other task states.
        });

        Scene splashScene = new Scene(splashLayout, Color.TRANSPARENT);
        splashScene.setFill(null);
        initStage.getIcons().add(new Image("file:src/FXML/icon/icon.png"));
        initStage.setScene(splashScene);
        initStage.initStyle(StageStyle.TRANSPARENT);
        initStage.setAlwaysOnTop(true);
        initStage.show();
    }

    public interface InitCompletionHandler {
        void complete();
    }
}