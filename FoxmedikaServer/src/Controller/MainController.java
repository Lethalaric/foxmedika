/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import java.net.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javax.swing.JOptionPane;
import org.h2.tools.Server;

/**
 * FXML Controller class
 *
 * @author Mukhtar
 */
public class MainController implements Initializable {
    @FXML
    private JFXTextField IPAddress;
    @FXML
    private Label Port;
    @FXML
    private JFXButton Start;
    private ArrayList<String> dummyData = new ArrayList<>(Arrays.asList("Nunc vel est ullamcorper, egestas leo nec, scelerisque mauris. Sed aliquam, quam fringilla ultrices faucibus, turpis est dictum nulla, quis viverra orci diam ut ligula. Quisque consectetur felis sit amet pharetra ultrices. Donec congue sodales sodales. In scelerisque est nec lacus aliquam, quis vehicula sapien convallis. Maecenas non vulputate sapien, ut lacinia urna. Duis finibus odio aliquam lacus rhoncus feugiat. Vivamus condimentum, risus a dapibus finibus, est lorem ullamcorper lorem, vulputate vulputate arcu mi eget felis. Donec ultrices quam sapien, non dictum nibh facilisis ut. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Mauris ultrices facilisis orci, eu eleifend augue tincidunt non. Suspendisse elementum nisi sed massa volutpat, quis vehicula lectus pulvinar. Mauris scelerisque convallis nunc non faucibus. Aliquam tincidunt diam at augue bibendum ullamcorper eu quis quam. Nulla tincidunt ex blandit, laoreet purus a, vehicula sem. Quisque quis odio lorem. Etiam convallis scelerisque quam, vel lobortis libero efficitur at. Integer eget interdum dui, id sagittis est. Praesent ut egestas neque. Cras a molestie sem. Nullam non eleifend libero. Integer vitae purus vitae sapien molestie aliquam. Integer dapibus tortor ac metus pharetra molestie. Suspendisse mi magna, sodales in nibh nec, consequat placerat libero. Proin eget dolor nibh. Nulla quis maximus ligula. Aliquam sed mi dolor. Nulla eu lacus ligula.".split(" "))) ;
    @FXML
    private Label status;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            // TODO
            status.setText("Server Non-aktif");
            isiIPAddress();
        } catch (UnknownHostException ex) {
            Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }    
    
    private void isiIPAddress() throws UnknownHostException {
//        String x = InetAddress.getLocalHost().toString();
        String x = InetAddress.getLocalHost().getHostAddress();
        IPAddress.setText(x);
        Port.setText(""+1527);
    }

    String ipaddress;
    File path;
    @FXML
    private void StartServer(ActionEvent event) throws Exception{
        try {
//            String ipaddress = JOptionPane.showInputDialog("Masukan IP address : ");
//            DirectoryChooser dc = new DirectoryChooser();
//            FileChooser fc = new FileChooser();
//            path = dc.showDialog(null);
            ipaddress = InetAddress.getLocalHost().getHostAddress();
//            System.setProperty("org.apache.derby.drda.startNetworkServer", "true");
//            System.setProperty("org.apache.derby.drda.host", ipaddress);
//            System.setProperty("org.apache.derby.drda.portNumber", ""+ 1527);
            System.out.println(ipaddress);
//            NetworkServerControl nsc = new NetworkServerControl(InetAddress.getLocalHost(), 5432);
//            nsc.start(null);
            Server server = Server.createTcpServer(null).start();
            TransferDatabase td = new TransferDatabase();
//            td.CreateConnection();
//            System.out.println(path.toString());
//            td.CreateConnection(path.toString());
            td.CreateConnection(""+InetAddress.getLocalHost().getHostAddress());
            System.out.println(InetAddress.getLocalHost().getHostAddress());
            td.CreateTable();
            setData(td);
            status.setText("Server Aktif");
//            td.SelectData("select * from ForTable");
//            ArrayList<String> hasil = td.data;
//            System.out.println(hasil.size());
//            System.out.println(hasil.get(3));
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, JOptionPane.ERROR_MESSAGE);
            Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void setData(TransferDatabase td) {
        for (int i = 0; i < dummyData.size()-13; i += 13) {
//            stat.executeUpdate("insert into ForTable values(1,'haris', '10 juli')" );
            td.InsertData("insert into TableBaru values('"+dummyData.get(i)+"','"+dummyData.get(i+1)+"','"+dummyData.get(i+2)+"','"+dummyData.get(i+3)+
                    "','"+dummyData.get(i+4)+"','"+dummyData.get(i+5)+"','"+dummyData.get(i+6)+"','"+dummyData.get(i+7)+"','"+dummyData.get(i+8)+"','"+
                    dummyData.get(i+9)+"','"+dummyData.get(i+10)+"','"+dummyData.get(i+11)+"','"+dummyData.get(i+12)+"')");
        }
        
    }

//    private void runSQL(ActionEvent event) {
//        TransferDatabase td = new TransferDatabase();
////            td.CreateConnection();
////        System.out.println(path.toString());
////        td.CreateConnection(path.toString());
////        System.out.println(ipaddress);
//        td.CreateConnection(ipaddress);
//        td.SelectData(SQLCOmmand.getText());
//        ArrayList<String> hasil = td.data;
//        System.out.println(hasil.size());
//        for (String s : hasil) {
//            System.out.println(s);
//        }
//    }
}


