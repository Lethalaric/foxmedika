/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.FadeTransition;
import javafx.animation.ParallelTransition;
import javafx.animation.ScaleTransition;
import javafx.animation.SequentialTransition;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.util.Duration;

/**
 * FXML Controller class
 *
 * @author Mukhtar
 */
public class PreloaderController implements Initializable {
    @FXML
    private Label nolnol;
    @FXML
    private Label nolsatu;
    @FXML
    private Label satunol;
    @FXML
    private Label satusatu;
    @FXML
    private Label started;
    @FXML
    private GridPane panel;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        
        satunol.setStyle("-fx-background-color : red");
        satusatu.setStyle("-fx-background-color : green");
        nolnol.setStyle("-fx-background-color : blue");
        nolsatu.setStyle("-fx-background-color : yellow");
        datalabel = new Label[]{nolnol, satunol, satusatu, nolsatu};
        
        running();
    }    
    
    private Label[] datalabel = null;
    
    private void running() {
        ScaleTransition[] dataST = new ScaleTransition[8];
        
        int counter = 0;
        
        for (int i = 0; i < 4; i++) {
//            datalabel[i].setStyle("-fx-background-color : blue");
            ScaleTransition temp1 = new ScaleTransition(Duration.millis(250), datalabel[i]);
            temp1.setByX(0.5f);
            temp1.setByY(0.5f);
            temp1.setCycleCount(1);
            
            ScaleTransition temp2 = new ScaleTransition(Duration.millis(250), datalabel[i]);
            temp2.setByX(-0.5f);
            temp2.setByY(-0.5f);
            temp2.setCycleCount(1);
            dataST[counter] = temp1;
            dataST[counter+1] = temp2;
            counter += 2;
        }
        
        FadeTransition ft = new FadeTransition(Duration.millis(1000), started);
        ft.setFromValue(1);
        ft.setToValue(0);
        ft.setAutoReverse(true);
        ft.setCycleCount(Timeline.INDEFINITE);
        
        SequentialTransition st = new SequentialTransition();
        st.setCycleCount(Timeline.INDEFINITE);
//        st.setAutoReverse(true);
        st.getChildren().addAll(dataST);
        
        ParallelTransition pt = new ParallelTransition();
        pt.getChildren().addAll(st, ft);
        pt.setCycleCount(Timeline.INDEFINITE);
        pt.play();
        
        new gantiScene().start();
        
        
    }
    
    class gantiScene extends Thread {
        public void run() {
            try {
                Thread.sleep(5000);
                Platform.runLater(new Runnable() {

                    @Override
                    public void run() {
                        Stage primaryStage = new Stage();
                        primaryStage = (Stage) panel.getScene().getWindow();
                        primaryStage.close();
                        primaryStage = new Stage();
                        Parent root = null;
                        try {
                            root = FXMLLoader.load(getClass().getResource("/FXML/Main.fxml"));
                        } catch (IOException ex) {
                            Logger.getLogger(PreloaderController.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        
                        FadeTransition ft2 = new FadeTransition(Duration.millis(1000), root);
                        ft2.setFromValue(0);
                        ft2.setToValue(1);
                        ft2.setCycleCount(1);
                        ft2.play();
                        Scene scene = new Scene(root);
                        primaryStage.setTitle("Formedika Server");
                        primaryStage.setScene(scene);
                        primaryStage.show();
                    }
                });
            } catch (Exception e) {
                
            }
        }
    }
}
