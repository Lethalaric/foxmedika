/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import java.sql.*;
import java.util.ArrayList;

/**
 *
 * @author Mukhtar
 */
public class TransferDatabase {
    
//    private String DBURL = "jdbc:derby://localhost:1527/myDB;create=true;user=me;password=mine";
//    private String DBURL = "jdbc:derby:myDB;create=true;user=me;password=mine";
    private String DBURL = "jdbc:derby:C:/Users/Mukhtar/Documents/NetBeansProjects/FoxmedikaServer/dist/myDB;create=true";
    private String tableName = "ForTable";
    private Connection conn = null;
    private Statement stat = null;
    
    public void CreateConnection() {
        try {
            Class.forName("org.apache.derby.jdbc.EmbeddedDriver").newInstance();
//            Class.forName("org.apache.derby.jdbc.ClientDriver").newInstance();
            conn = DriverManager.getConnection(DBURL);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
//    public void CreateConnection(String value) {
//        String DBURL = "jdbc:derby://"+value+"/myDB;create=true";
//        try {
////            Class.forName("org.apache.derby.jdbc.EmbeddedDriver").newInstance();
//            Class.forName("org.apache.derby.jdbc.ClientDriver").newInstance();
//            conn = DriverManager.getConnection(DBURL);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
    
    public void CreateConnection(String value) {
        String DBURL = "jdbc:h2:tcp://"+value+"/~/myDB";
        try {
//            Class.forName("org.apache.derby.jdbc.EmbeddedDriver").newInstance();
            Class.forName("org.h2.Driver");
            conn = DriverManager.getConnection(DBURL);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void CreateTable() {
        try {
            DatabaseMetaData dbm = conn.getMetaData();
            // check if "employee" table is there
            ResultSet tables = dbm.getTables(null, null, "TABLEBARU", null);
            stat = conn.createStatement();
            int count = 0;
            if (tables.next()) {
                count = stat.executeUpdate(
                "DROP TABLE TableBaru");
            }
            String sql = "CREATE TABLE TableBaru (a1 VARCHAR(50), a2 VARCHAR(50), a3 VARCHAR(50), a4 VARCHAR(50), a5 VARCHAR(50),"
                    + " a6 VARCHAR(50), a7 VARCHAR(50), a8 VARCHAR(50), a9 VARCHAR(50), a10 VARCHAR(50), a11 VARCHAR(50), a12 VARCHAR(50), a13 VARCHAR(50))";
//                count = stat.executeUpdate(
//                "CREATE TABLE ForTable (nomor INT, nama VARCHAR(20),"
//                + " TTL VARCHAR(20))");
            stat.executeUpdate(sql);
            System.out.println("berhasil terbuat");
            stat.close();        

//            conn.close();
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void InsertData(String sql) {
        try {
            stat = conn.createStatement();
            conn.setAutoCommit(false);
            stat.executeUpdate(sql);
//            stat.executeUpdate("insert into ForTable values(1,'haris', '10 juli')" );
            stat.close();
            conn.commit();
        } catch (Exception e) {
            e.printStackTrace();
        } 
    }
    
    ArrayList<String> data = new ArrayList<>();
    
    public void SelectData(String sql) {
        try {
            stat = conn.createStatement();
            ResultSet rs = stat.executeQuery(sql);
            while (rs.next()) {
                data.add(""+rs.getString("a1"));
//                data.add(""+rs.getString("nama"));
//                data.add(""+rs.getString("TTL"));
            }
            rs.close();
            stat.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void SelectData(String sql, String[] parameter) {
        try {
            stat = conn.createStatement();
            ResultSet rs = stat.executeQuery(sql);
            for (String s : parameter) {
                System.out.println(s);
            }
            while (rs.next()) {
                for (String s : parameter) {
                    data.add(""+rs.getString(s));
                }
            }
            rs.close();
            stat.close();
            conn.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
