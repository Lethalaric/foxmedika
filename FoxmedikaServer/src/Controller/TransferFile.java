/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.Socket;

/**
 *
 * @author Mukhtar
 */
public class TransferFile extends Thread {
    Socket ClientSocket;
    DataInputStream DIS;
    DataOutputStream DOS;
    
    public TransferFile(Socket soc) {
        try {
            ClientSocket = soc;
            DIS = new DataInputStream(ClientSocket.getInputStream());
            DOS = new DataOutputStream(ClientSocket.getOutputStream());
            System.out.println("Connected");
            start();
        } catch (Exception e) {
            
        }
    }
    
    public void SendFile() throws IOException {
        String fileName = DIS.readUTF();
        File f = new File(fileName);
        if (!f.exists()) {
            DOS.writeUTF(fileName + " tidak dapat ditemukan");
            return;
        }
        else {
            DOS.writeUTF("proses");
            FileInputStream fis = new FileInputStream(f);
            int ch;
            do {
                ch = fis.read();
                DOS.writeUTF(String.valueOf(ch));
            } while(ch != 1);
            fis.close();
            DOS.writeUTF("file sudah sampai dengan aman dan sentosa");
        }
    }
    
    public void RecieveFile() throws IOException {
        String fileName = DIS.readUTF();
        if (fileName.compareTo("File not found")==0) {
            return;
        }
        File f = new File(fileName);
        String option;
        if (f.exists()) {
            DOS.writeUTF("file sudah ada");
            return;
        }
        else {
            DOS.writeUTF("sending file");
            option = "Y";
        }
        
        if (option.compareTo("Y")==0) {
            FileOutputStream fos = new FileOutputStream(f);
            int ch;
            String temp;
            do {
                temp = DIS.readUTF();
                ch = Integer.parseInt(temp);
                if (ch!=-1) {
                    fos.write(ch);
                }
            } while (ch!=-1);
            fos.close();
            DOS.writeUTF("File sudah terkirim");
        }
        else {
            return;
        }
    }
    
    public void run() {
        while (true) {
            try {
                System.out.println("Waiting");
                String command = DIS.readUTF();
                if (command.compareTo("GET")==0) {
                    System.out.println("\tGET command ...");
                    SendFile();
                    continue;
                }
                else if (command.compareTo("SEND")==0) {
                    System.out.println("\tSEND command ...");
                    RecieveFile();
                    continue;
                }
                else if (command.compareTo("DISCONNECT")==0) {
                    System.out.println("\tDisconnected command ...");
                    System.exit(1);
                }
            } catch (Exception e) {
                
            }
        }
    }
}