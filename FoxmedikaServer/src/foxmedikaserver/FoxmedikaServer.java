/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package foxmedikaserver;

import com.sun.javafx.application.LauncherImpl;
//import foxmedikapreloader.FoxmedikaPreloader;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 *
 * @author Mukhtar
 */
public class FoxmedikaServer extends Application {
    
    @Override
    public void start(Stage primaryStage) throws IOException {
        primaryStage = new Stage(StageStyle.UNDECORATED);
        Parent root = FXMLLoader.load(getClass().getResource("/FXML/Preloader.fxml"));
        Scene scene = new Scene(root);
        scene.setFill(null);
        primaryStage.initStyle(StageStyle.TRANSPARENT);
        primaryStage.setTitle("Formedika Server");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
//        LauncherImpl.launchApplication(FoxmedikaServer.class, FoxmedikaPreloader.class, args); 
    }
    
}
